package com.synchronoss.test;

import com.synchronoss.core.PropertyHelper;
import com.synchronoss.test.untils.ContactsInfo;
import com.synchronoss.test.untils.EmailMessage;
import com.synchronoss.test.untils.TestCase;
import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Composer_Mail_Related_Testcases extends TestCase {
	
	String emailaddress1, emailaddress2, emailaddress3;
	ContactsInfo contactsinfo1, contactsinfo2, contactsinfo3;
	EmailMessage msg;
	
    public String getTestDescription() {
    	return "Input single valid email in all field";
    }

    @Test(priority = 1,groups = {"sanity", "composer", "pailwan"})
    public void InputSingleValidRecipientInAllField()
    {
        emailaddress1 = PropertyHelper.configParams.getPropertyValue("username1");
        emailaddress2 = PropertyHelper.configParams.getPropertyValue("username2");
        emailaddress3 = PropertyHelper.configParams.getPropertyValue("username3");
        contactsinfo1 = new ContactsInfo(emailaddress1);
        contactsinfo2 = new ContactsInfo(emailaddress2);
        contactsinfo3 = new ContactsInfo(emailaddress3);
        msg = new EmailMessage();

        navigation.clickContactsTab();

        // clean up all contacts
        contacts.cleanupAllContacts();

        // add a new contact contactsinfo2 into "Default" address book
        contacts.clickOnDefaultAddressBook();
        contactsutil.addNewContact(contactsinfo2);

        // verify this contact exist or not
        Assert.assertTrue(contacts.verifyContactInListByEmail(contactsinfo2.email));

        navigation.clickMailTab();

        // input email address into all 3 fields, To field and Cc field and Bcc field
        // click "Compose" button to open compose window
        email.clickEmailToolBarButton("Compose");
        // input contactsinfo2.firstname to To field
        email.getComposer().typeRecipientWithoutValidation("To", contactsinfo2.firstname, Keys.ENTER);
        //  email.getComposer().clickOnCcButton();
        // input contactsinfo1.email to Cc field
        email.getComposer().typeRecipientWithoutValidation("Cc", contactsinfo1.email, Keys.ENTER);

        // input contacsinfo3.email to Bcc field
        email.getComposer().clickOnBccButton();
        email.getComposer().typeRecipientWithoutValidation("Bcc", contactsinfo3.email, Keys.ENTER);

        // verify the three contacts' email exist in 3 fields
        Assert.assertTrue(email.getComposer().verifyRecipientInField(contactsinfo2.firstname));
        Assert.assertTrue(email.getComposer().verifyRecipientInField(contactsinfo1.email));
        Assert.assertTrue(email.getComposer().verifyRecipientInField(contactsinfo3.email));

        // send message
        email.getComposer().typeInSubjectField(msg.subject);
        email.getComposer().typeInMessageBody(msg.body);
        email.getComposer().clickComposerToolBarButton("Send");
    }

    @Test(dependsOnMethods = {"InputSingleValidRecipientInAllField"})
    public void InputMultipleValidEmailInToField()
    {
        navigation.clickMailTab();

        // input email address into all 3 field
        email.clickEmailToolBarButton("Compose");
        email.getComposer().typeRecipientWithoutValidation("To", contactsinfo2.firstname, Keys.ENTER);
        email.getComposer().typeRecipientWithoutValidation("To", contactsinfo1.email, Keys.ENTER);
        email.getComposer().typeRecipientWithoutValidation("To", contactsinfo2.email, ",");

        Assert.assertTrue(email.getComposer().verifyRecipientInField(contactsinfo2.firstname));
        Assert.assertTrue(email.getComposer().verifyRecipientInField(contactsinfo1.email));
        Assert.assertTrue(email.getComposer().verifyRecipientInField(contactsinfo2.email));

        // send message
        msg = new EmailMessage();
        email.getComposer().typeInSubjectField(msg.subject);
        email.getComposer().typeInMessageBody(msg.body);
        email.getComposer().clickComposerToolBarButton("Send");
    }

    @Test(dependsOnMethods = {"InputSingleValidRecipientInAllField"})
    public void InputMultipleValidRecipientsInCcField()
    {
        if(!email.verifyEmailToolBarButtonEnable("Compose"))
        {
            email.emailComposer.clickMinimizeOrCloseComposeEmailWindow("close");
        }
        navigation.clickMailTab();

        // input three email address into Cc field
        email.clickEmailToolBarButton("Compose");
        //email.getComposer().clickOnCcButton();
        email.getComposer().typeRecipientWithoutValidation("Cc", contactsinfo2.firstname, Keys.ENTER);
        email.getComposer().typeRecipientWithoutValidation("Cc", contactsinfo1.email, Keys.ENTER);
        email.getComposer().typeRecipientWithoutValidation("Cc", contactsinfo2.email, ",");

        // verify the three email adddress exist in Cc field
        Assert.assertTrue(email.getComposer().verifyRecipientInField(contactsinfo2.firstname));
        Assert.assertTrue(email.getComposer().verifyRecipientInField(contactsinfo1.email));
        Assert.assertTrue(email.getComposer().verifyRecipientInField(contactsinfo2.email));

        // send message
        email.getComposer().typeRecipientWithoutValidation("To", contactsinfo2.firstname, Keys.ENTER);
        msg = new EmailMessage();
        email.getComposer().typeInSubjectField(msg.subject);
        email.getComposer().typeInMessageBody(msg.body);
        email.getComposer().clickComposerToolBarButton("Send");
    }

}
