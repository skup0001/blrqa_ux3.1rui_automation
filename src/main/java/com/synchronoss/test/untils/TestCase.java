package com.synchronoss.test.untils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.synchronoss.core.*;
import org.openqa.selenium.WebDriverException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 * @author Jerry Zhang, Edmund Maruhn
 * @description 
 * This class encapsulates page object utility objects and is the super
 * class of all test case classes.
 * Mothods of setup, teardown and TestNG parameter manipulations were implemented 
 * by Edmund Maruhn on on 26.06.2015 in Rich UI old framework and is ported to 
 * this framework.
 */
public abstract class TestCase extends Base {
	
	//#########################################################################
	//							Email Utility Objects
	//#########################################################################
	public EmailUtils emailutl = new EmailUtils();
	public ContactsUtils contactsutil = new ContactsUtils();
	public CalendarUtils calendarutil = new CalendarUtils();
	public TasksUtils tasksutl = new TasksUtils();

	
	
	//#########################################################################
	//						Methods Printing Test Info
	//#########################################################################
	public abstract String getTestDescription();
    
    // Prints login user/pass.
    private void loginParams() {
    	Logging.params("Initial Login User: " + this.loginUser);
    	Logging.params("Initial Login Pass: " + this.loginPass);
    }
    
    // Prints the entire test information. Should be called by each test at starting point.
    protected void printTestInfo() {
    	Logging.description("Test Started: " + this.getClass().getSimpleName());
    	Logging.description(this.getTestDescription());
    	this.loginParams();
    }
	
	
    
	//#########################################################################
	//					Internal&External Parameters Manipulation
	//#########################################################################
    
	// parameters used by member methods of any subclass test cases.
	public List<Object> classParameters = new ArrayList<Object>();
	
	// external testNG parameters
    private Map<String, String> testNGParameters;
    
    // always keep in same order then in @Parameters
    private static final String[] parameterNames = new String[] {
        "emailid1", "username1", "password1",
        "emailid2", "username2", "password2",
        "emailid3", "username3", "password3",
        "emailid4", "username4", "password4",
        "userInterface", "url"
    };

    /*
    @BeforeMethod(alwaysRun = true)
    @Parameters({"emailid1", "username1", "password1", 
    		"emailid2", "username2", "password2", 
            "emailid3", "username3", "password3",
            "emailid4", "username4", "password4",
            "userInterface", "browser.url"})
    public void setupParameters(String emailid1, String username1, String password1,
                                String emailid2, String username2, String password2,
                                String emailid3, String username3, String password3,
                                String emailid4, String username4, String password4,
                                String userInterface, String url) {
    	
        // always keep in same order with @Parameters
        Map<String,String> map = this.setupFromArguments(
            emailid1, username1, password1,
            emailid2, username2, password2,
            emailid3, username3, password3,
            emailid4, username4, password4,
            userInterface, url
        );

        this.updateFromUserPoolIfNeeded(map);
        this.testNGParameters = map;
        System.out.println("jfang TestCase.before method.setupParameters");
    }

    private void updateFromUserPoolIfNeeded(Map<String, String> map) {
        if (Base.usingUserPool) {
            this.updateFromUserPool(map);
        }
    }
    */

    /*
	private void updateFromUserPool(Map<String, String> map) {

		// edmund.maruhn: NOTICE
		// a more generic solution would be better here, but requires UserPool
		// to be restructured
		if (!Base.userLocalExcel) {
			map.put("emailid1", mxosPool.getEmailId1());
			map.put("username1", mxosPool.getUserName1());
			map.put("password1", mxosPool.getPassword1());

			map.put("emailid2", mxosPool.getEmailId2());
			map.put("username2", mxosPool.getUserName2());
			map.put("password2", mxosPool.getPassword2());

			map.put("emailid3", mxosPool.getEmailId3());
			map.put("username3", mxosPool.getUserName3());
			map.put("password3", mxosPool.getPassword3());

			map.put("emailid4", mxosPool.getEmailId4());
			map.put("username4", mxosPool.getUserName4());
			map.put("password4", mxosPool.getPassword4());
		} else {
			map.put("emailid1", pool.getEmailId1());
			map.put("username1", pool.getUserName1());
			map.put("password1", pool.getPassword1());

			map.put("emailid2", pool.getEmailId2());
			map.put("username2", pool.getUserName2());
			map.put("password2", pool.getPassword2());

			map.put("emailid3", pool.getEmailId3());
			map.put("username3", pool.getUserName3());
			map.put("password3", pool.getPassword3());

			map.put("emailid4", pool.getEmailId4());
			map.put("username4", pool.getUserName4());
			map.put("password4", pool.getPassword4());

		}
	}
	*/

    private Map<String, String> setupFromArguments(String... arguments) {
        Map<String, String> map = new HashMap<String, String>();

        for (int index = 0; index < TestCase.parameterNames.length; ++index) {
            map.put(TestCase.parameterNames[index], arguments[index]);
        }

        return map;
    }

    public final String getParameter(String key) {
        return testNGParameters != null ? testNGParameters.get(key) : null;
    }
    
    
    
	//#########################################################################
	//						Test Setup, Run, and Tear-down
	//#########################################################################
    /*@Test(alwaysRun = true)
    public void run() {
    	this.printTestInfo();
        try {
        	System.out.println("jfang run in testcase.run");
            this.doTest();
            // passes all expected
            this.markAsPassed();
        } catch (WebDriverException ex) {
        	Logging.error(ex.getMessage());
        	Tools.captureFullStackTrace(ex);
        } finally {
            this.handleTestResult();
        }
        System.out.println("jfang TestCase.@Test");
    }
    */


    private void handleTestResult() {
        String status = this.getClass().getSimpleName() + ": ";
        status += isPassed() ? "PASS" : "FAIL";
        Logging.status(status);
        
    	// take a screenshot if the test fails
    	if (status.toLowerCase().contains("fail")) {
    		Tools.captureScreen(this.getClass().getSimpleName() + "-Test-FailurePoint");
    	}
    }


    /**
     * Actual test code goes here. When test has been completed successfully the markAsPassed() method must be invoked
     * to get the test logged as passed properly.
     */
    //protected abstract void doTest();



    private boolean passed = false;

    public final void markAsPassed() {
        this.passed = true;
    }

    public final boolean isPassed() {
        return passed;
    }

    //(alwaysRun = true, dependsOnMethods = { "setupParameters" })

    /*
    @BeforeMethod
    public void setup() {
    	try {
    		System.out.println("jfang hhhh");
    		doTestSetup();
    	} catch (WebDriverException ex) {
    		Logging.error("Test setup process failed");
    		// take a screenshot
    		Tools.captureScreen(this.getClass().getSimpleName() + "-Setup-FailurePoint");
    		throw ex;
    	}
    	System.out.println("jfang TestCase.before method.setup");
    }
    */

    /**
     * Override this method if your test needs to perform any test data setup before actual test execution.
     * Note: do not forget to call super.doTestSetup() first
     *
     * @throws InterruptedException
     */
    protected void doTestSetup() {
    	// user login
		login.typeUsername(this.loginUser);
		login.typePassword(this.loginPass);
		login.clickLoginButton();

        Logging.info("Do test data setup");
    }


    /*
    @AfterMethod(alwaysRun = true)
    public void teardown() {
    	try {
            doTestCleanup();
    	} catch (WebDriverException ex) {
    		Logging.error("Test teardown process failed");
    		// take a screenshot
    		Tools.captureScreen(this.getClass().getSimpleName() + "-TearDown-FailurePoint");
    		throw ex;
    	}
    	System.out.println("jfang TestCase.after method");
    }
    */

    /**
     * Override this method if your test needs to perform any test data cleanup after test has been executed.
     * Note: do not forget to call super.doTestCleanup() first
     *
     * @throws InterruptedException
     */
    protected void doTestCleanup() {
        Logging.info("Do test data cleanup");
    }
}
