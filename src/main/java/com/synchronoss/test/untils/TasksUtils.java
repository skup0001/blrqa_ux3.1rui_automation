package com.synchronoss.test.untils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import com.synchronoss.core.Base;
import com.synchronoss.core.Logging;
import com.synchronoss.core.Tools;
import com.synchronoss.pageobject.CoreTasks;
public class TasksUtils extends Base {

	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 11, 2016<br>
	 * <b>Description: </b><br>
	 * Creates a new task with simply adding the title.
	 * 
	 * @return the new task object created
	 */
	public TaskItem createSimpleTask() {
		TaskItem task = new TaskItem();

		tasks.clickOnTaskAddButton();
		tasks.typeInDetailTextField("Title", task.title);
		tasks.clickToolbarButtonInDetail("Save");
		tasks.waitForLoadMaskDismissed();

		return task;
	}

	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 26, 2016<br>
	 * <b>Description: </b><br>
	 * Edits and task contents without clicking on save button. The task should
	 * already open for editing. Only non-null fields will be added to the task.
	 */
	public void editTaskFieldsWithoutSaving(TaskItem task, String timeFormat) {

		tasks.typeInDetailTextField("Title", task.title);

		if (task.list != null) {
			tasks.selectOptionInDetailComboBox("List", task.list);
		}
 
		if (task.priority != null) {
			tasks.selectOptionInDetailComboBox("Priority", task.priority);
		}
		
		if (task.status != null) {
			tasks.selectOptionInDetailComboBox("Status", task.status);
		}
		if (task.due != null) {
			tasks.checkOrUncheckDueDateInDetail(true);
			tasks.selectDueDateInDetail(task.due.toLocalDate());
			tasks.selectDueTimeInDetail(task.due.toLocalTime(), timeFormat);
		} else {
			tasks.checkOrUncheckDueDateInDetail(false);
		}
		if (task.reminder != null) {
			tasks.selectOptionInDetailComboBox("Reminder", task.reminder);
		}
		if (task.url != null) {
			tasks.typeInDetailTextField("Url", task.url);
		}
		if (task.description != null) {
			tasks.typeInDetailTextField("Description", task.description);
		}
	}

	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 20, 2016<br>
	 * <b>Description: </b><br>
	 * Creates a new task based on the Task object provided. Only non-null
	 * fields will be added to the task.
	 */
	public void createTask(TaskItem task, String timeFormat) {
		tasks.clickOnTaskAddButton();
		this.editTask(task, timeFormat);
	}

	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 25, 2016<br>
	 * <b>Description: </b><br>
	 * Edits and saves task content. The task should already open for editing.
	 * Only non-null fields will be added to the task.
	 */
	public void editTask(TaskItem task, String timeFormat) {
		
		this.editTaskFieldsWithoutSaving(task, timeFormat);
		tasks.clickToolbarButtonInDetail("Save");
		tasks.waitForLoadMaskDismissed();
	}
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 30, 2016<br>
	 * <b>Description: </b><br>
	 * Creates a number of tasks with random field value.
	 * 
	 * @param taskCount - the number of tasks to create
	 */
	public List<TaskItem> createRandomTasks(int taskCount, String timeFormat) {
		
		ArrayList<TaskItem> items = new ArrayList<TaskItem>(); 
		
		ArrayList<String> priorities = new ArrayList<String>();
		priorities.add(CoreTasks.PRIORITY_HIGH);
		priorities.add(CoreTasks.PRIORITY_LOW);
		priorities.add(CoreTasks.PRIORITY_NORMAL);
		
		//LocalDateTime baseDT = LocalDateTime.now();
		LocalDateTime baseDT = tasks.getBrowserDateTime();
		Logging.info(baseDT.toString());
		Random rand = new Random();
		
		for (int i = 0; i < taskCount; i++) {
			// Create a task with random title.
			TaskItem task = new TaskItem();
			// Set a random priority.
			task.priority = priorities.get(rand.nextInt(priorities.size()));
			// Set a random due with sparse distribution.
			LocalDate date = baseDT.toLocalDate().plusDays(rand.nextInt(2 * taskCount));
			Logging.info(baseDT.toLocalTime().toString());
			LocalTime time = tasks.getNextIntegralMinute(
					baseDT.toLocalTime().plusMinutes(3).plusMinutes(rand.nextInt(15 * taskCount)));
			Logging.info(time.toString());
			task.due = LocalDateTime.of(date, time);
			
			items.add(task);
			
			this.createTask(task, timeFormat);
		}
		return items;
	}

}
