package com.synchronoss.test.untils;

import com.synchronoss.core.Base;
import com.synchronoss.core.Logging;

/**
 * @author Jerry Zhang
 * @description 
 * This class encapsulates page object utility objects and is the super
 * class of all test case classes.
 */
public class TestCaseBase extends Base {
	// Email utility object.
	public EmailUtils emailutl = new EmailUtils();
	public ContactsUtils contactsutil = new ContactsUtils();
	
	// Temporarily placed these methods here, to be removed in the future.
    // Prints test case description. Should be overridden by all tests.
    protected void testDescription() {
    	Logging.description("No appropriate description for this test case");
    }
    
    // Prints login user/pass.
    private void loginParams() {
    	Logging.params("Login User: " + this.loginUser);
    	Logging.params("Login Pass: " + this.loginPass);
    }
    
    // Prints the entire test information. Should be called by each test at starting point.
    protected void printTestInfo() {
    	Logging.description("Test Started: " + this.getClass().getSimpleName());
    	this.testDescription();
    	this.loginParams();
    }
}
