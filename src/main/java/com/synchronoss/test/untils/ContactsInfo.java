package com.synchronoss.test.untils;

import java.util.UUID;

public class ContactsInfo {
  public String firstname = null;
  public String middlename = null;
  public String lastname = null;
  public String nickname = null;

  public String email = null;
  public String mobile = null;
  public String phone = null;

  public String street = null;
  public String city = null;
  public String state = null;
  public String zip = null;
  public String country = null;

  public ContactsInfo(String email) {
    this.email = email;
    this.firstname = "Fname" + UUID.randomUUID().toString().substring(0, 4);
    this.lastname = "Lname" + UUID.randomUUID().toString().substring(0, 4);

    // to be added.
  }

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public String getMiddlename() {
    return middlename;
  }

  public void setMiddlename(String middlename) {
    this.middlename = middlename;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public String getNickname() {
    return nickname;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getZip() {
    return zip;
  }

  public void setZip(String zip) {
    this.zip = zip;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

}
