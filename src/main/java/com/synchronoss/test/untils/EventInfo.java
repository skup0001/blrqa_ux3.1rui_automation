package com.synchronoss.test.untils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.synchronoss.core.Base;

public class EventInfo extends Base{
	
	public static final String REPEATABLE_NEVER = "Never";
	public static final String REPEATABLE_DAILY ="Daily";
	public static final String REPEATABLE_WEEKLY ="Weekly";
	public static final String REPEATABLE_MONTHLY ="Monthly";
	public static final String REPEATABLE_YEARLY ="Yearly";
	public static final String GENERAL = "General";
	public static final String INVITE = "Invite";
	public static final String WORK = "Work";
	public static final String SCHOOL = "School";
	public static final String RED = "Red";
	public static final String YELLOW = "Yellow";
	public static final String GREEN = "Green";
	public static final String BIRTHDAY = "Birthday";
	public static final String ANNIVERSARY = "Anniversary";
	public static final String DATE = "Date";
	public static final String VACATION = "Vacation";
	public static final String FUN = "Fun";
	public static final String BILLS = "Bills";
	public static final String PHONE = "Phone";
	public static final String DOCTOR = "Doctor";
	public static final String FLAG = "Flag";
	public static final String PET = "Pet";
	public static final String SPORT = "Sport";


	private String summary;
	private LocalDateTime startDateTime;
	private LocalDateTime endDateTime;
	private boolean allDay;
	private String repeatable;
	private String reminderTime;
	private String destCalendar;
	private String category;
	private String location;
	private String description;
	private List<String> attendeeList = new ArrayList<String>();

	/**
	 * <b>Author</b>: Lynn Li<br>
	 * <b>Date</b>: Oct 16, 2015<br>
	 * <b>Description</b><br>
	 * Initializes a newly created eventInfo object.
	 */
	public EventInfo() {
		summary = "summary" + UUID.randomUUID().toString().substring(0, 8);
		startDateTime = calendar.getIntegralMinute(LocalDateTime.now());
		endDateTime = startDateTime.plusMinutes(60);
		allDay = false;
		repeatable = REPEATABLE_NEVER;
		reminderTime = "None";
		destCalendar = "default";
		category = GENERAL;
		location = "Beijing";
		description = "test event in description part";
		attendeeList = null;
	}
	
	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Sep 22, 2015<br>
	 * <b>Description</b><br>
	 * Initializes a newly created eventInfo object.
	 * 
	 * <b>Parameters</b><br>
	 * String email - will be used when you select the calendar name
	 * String eventName  - will be used to create event name
	 * 
	 */
	public EventInfo(String email) {
		summary = "summary" + UUID.randomUUID().toString().substring(0, 8);
		startDateTime = calendar.getIntegralMinute(LocalDateTime.now());
		endDateTime = startDateTime.plusMinutes(60);
		allDay = false;
		repeatable = REPEATABLE_NEVER;
		reminderTime = "None";
		destCalendar = email;
		category = GENERAL;
		location = "Beijing";
		description = "test event in description part";
		attendeeList = null;
	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Sep 22, 2015<br>
	 * <b>Description</b><br>
	 * Initializes a newly created eventInfo object.
	 * 
	 * <b>Parameters</b><br>
	 * String email - will be used when you select the calendar name
	 * String eventName  - will be used to create event name
	 * int offsetStartDay - will be used to set offset of start day
	 * int offsetEndDay - will be used to set offset of end day
	 * int offsetMinuteStartTime - will be used to set offset of start time
	 * int offsetMinuteEndTime - will be used to set offset of end time
	 */
	public EventInfo(String email, String eventName, int offsetStartDay,
			int offsetEndDay,int offsetMinuteStartTime, int offsetMinuteEndTime) {
		summary = eventName + UUID.randomUUID().toString().substring(0, 8);
		startDateTime = calendar.getIntegralMinute(LocalDateTime.now()).plusDays(offsetStartDay).plusMinutes(offsetMinuteStartTime);
		endDateTime = startDateTime.plusDays(offsetEndDay).plusMinutes(offsetMinuteEndTime);
		allDay = false;
		repeatable=REPEATABLE_NEVER;
		reminderTime = "None";
		destCalendar = email;
		category = GENERAL;
		location = "Beijing";
		description = "this is the in description part";
		attendeeList = null;

	}
	
	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Sep 22, 2015<br>
	 * <b>Description</b><br>
	 * Initializes a newly created eventInfo object.
	 * 
	 * <b>Parameters</b><br>
	 * String email - will be used when you select the calendar name
	 * String eventName  - will be used to create event name
	 */
	public EventInfo(String email, String eventName) {
		summary = eventName;
		startDateTime = calendar.getIntegralMinute(LocalDateTime.now());
		endDateTime = startDateTime.plusMinutes(60);
		allDay = false;
		repeatable=REPEATABLE_NEVER;
		reminderTime = "None";
		destCalendar = email;
		category = GENERAL;
		location = "Beijing";
		description = "test event in description part";
		attendeeList = null;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public LocalDateTime getStartDateTime() {
		return startDateTime;
	}

	public void setStartDateTime(LocalDateTime startDateTime) {
		this.startDateTime =startDateTime;
	}

	public LocalDateTime getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(LocalDateTime endDateTime) {
		this.endDateTime = endDateTime;
	}

	public String getRepeatable() {
		return repeatable;
	}

	public void setRepeatable(String repeatable) {
		this.repeatable = repeatable;
	}
	
	public String getReminder() {
		return reminderTime;
	}

	public void setReminder(String reminderTime) {
		this.reminderTime = reminderTime;
	}

	public String getDestCalendar() {
		return destCalendar;
	}

	public void setDestCalendar(String destCalendar) {
		this.destCalendar = destCalendar;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean getAllDay() {
		return allDay;
	}

	public void setAllDay(boolean allDay) {
		this.allDay = allDay;
	}
	
	public List<String> getAttendee() {
		return attendeeList;
	}

	public void setAttendee(List<String> attendeeList) {
		this.attendeeList = attendeeList;
	}
	
}
