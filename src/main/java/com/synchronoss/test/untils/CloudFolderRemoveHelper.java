package com.synchronoss.test.untils;

import java.io.IOException;
import java.util.HashMap;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

public class CloudFolderRemoveHelper {
  public static void main(String[] args) {
    String userName = "56765432111";
    System.out.println(CloudFolderRemoveHelper.deleteFolder(CloudFolderRemoveHelper.getAccessTokenAndLcId(userName), "123"));
  }

  /**
   * Get the access token of the personal cloud
   * 
   * @param userName - the user name of the cloud
   * @author Fiona
   * @return HashMap<String, String> - the keywords for the hashmap are : "acess_token", and "lcid"
   */
  public static HashMap<String, String> getAccessTokenAndLcId(String userName) {
    HashMap<String, String> tokenID = new HashMap<String, String>();
    String accessToke = null;
    String lcid = null;
    OkHttpClient client = new OkHttpClient();
    MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
    RequestBody body = RequestBody.create(mediaType,
        "client_id=" + userName + "&client_secret=p%40ssw0rd&grant_type=client_credentials");
    Request request = new Request.Builder()
        .url("https://common-aws1-us-east-1-qa-wlpc.cloud.synchronoss.net/atp/oauth2/sessions")
        .post(body).addHeader("content-type", "application/x-www-form-urlencoded")
        .addHeader("cache-control", "no-cache").build();
    try {
      Response response = client.newCall(request).execute();
      String testResponse = response.body().string();
      response.body().close();
      System.out.println(testResponse);
      String[] responseCode = testResponse.split("\":\"");
      accessToke = responseCode[1].substring(0, responseCode[1].indexOf("\""));
      lcid = responseCode[3].substring(0, responseCode[3].indexOf("\""));
      System.out.println(lcid);
      System.out.println(accessToke);

    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    tokenID.put("acess_token", accessToke);
    tokenID.put("lcid", lcid);

    return tokenID;

  }

  /**
   * Delete the folder in personal cloud
   * 
   * @param tokenInfo - the token access info and lcid info
   * @param folderName - the folder that needs to be deleted
   * 
   * @author Fiona
   * @return int - the execution status
   */
  public static int deleteFolder(HashMap<String, String> tokenInfo, String folderName) {
    int requestStatus = 0;
    OkHttpClient client = new OkHttpClient();

    Request request = new Request.Builder()
        .url("https://common-aws1-us-east-1-qa-wlpc.cloud.synchronoss.net/dv/api/user/"
            + tokenInfo.get("lcid") + "/repository/SyncDrive/folder?path=%2F" + folderName
            + "&purge=true")
        .delete(null).addHeader("accept", "application/vnd.newbay.dv-1.18+xml")
        .addHeader("authorization",
            "NWB token=\"" + tokenInfo.get("acess_token") + "\" authVersion=\"1.0\"")
        .addHeader("cache-control", "no-cache").build();

    try {
      // delete the folder
      Response response = client.newCall(request).execute();
      requestStatus = response.code();
      response.body().close();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    
    if (requestStatus == 200) {
      System.out.println("Delete folder " + folderName + " in cloud is done.");
    } else {
      System.out.println("Delete folder " + folderName + " in cloud failed!!!");
    }

    return requestStatus;
  }


}
