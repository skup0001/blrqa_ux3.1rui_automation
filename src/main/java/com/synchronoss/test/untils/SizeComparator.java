package com.synchronoss.test.untils;

import java.util.Comparator;

public class SizeComparator implements Comparator<String> {
  // a negative integer, zero, or a positive integer as the first argument is less than, equal to,
  // or greater than the second.

  @Override
  public int compare(String size1, String size2) {
    double compareSize1 = 0.0;
    double compareSize2 = 0.0;

    int result = 0;
    compareSize1 = convertStringToDouble(size1);
    compareSize2 = convertStringToDouble(size2);

    if (compareSize1 > compareSize2)
      result = 1;
    else if (compareSize1 < compareSize2)
      result = -1;

    return result;
  }

  /***
   * Convert all input into bytes for comparing
   * 
   * @param converStirng
   * @return long
   */
  private long convertStringToDouble(String converStirng) {
    long result;
    // remove the bracket and convert all strings to lower case
    converStirng = converStirng.toLowerCase().replaceAll("\\p{P}", "");

    // check if size have mb, then turn it into byte by *1024*1024
    if (converStirng.contains("mb")) {
      converStirng = converStirng.replaceAll("[a-z]", "");
      result = Long.parseLong(converStirng) * 1024 * 1024;
    } else if (converStirng.contains("kb")) {
      converStirng = converStirng.replaceAll("[a-z]", "");
      result = Long.parseLong(converStirng) * 1024;
    } else {
      converStirng = converStirng.replaceAll("[a-z]", "");
      result = Long.parseLong(converStirng);
    }
    return result;

  }

/*
 * test class
  public static void main(String[] args) {
    SizeComparable sc = new SizeComparable();
    sc.compare("(2050MB)", "(2051kb)");
    sc.compare("(2050b)", "(2051kb)");
    sc.compare("(2050kb)", "(2051kb)");
    sc.compare("(2050MB)", "(2050MB)");
  }
*/

}
