package com.synchronoss.pageobject;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import com.synchronoss.core.Base;
import com.synchronoss.core.Logging;
import com.synchronoss.core.PropertyHelper;
import com.synchronoss.core.Tools;

public class CoreLogin extends PageObjectBase {

	/**
	 * <b>Author</b>: <br>
	 * <b>Date</b>: <br>
	 * <b>Description</b><br>
	 * 
	 * @param 
	 * @return 
	 */
	public void typeUsername(String username) {
		Logging.info("Typing login user name: " + username);
		String login_username_field = PropertyHelper.coreLoginFile
				.getPropertyValue("login_username_field");
		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(login_username_field))
					.sendKeys(username.trim());
		} catch (WebDriverException ex) {
			Logging.error("Could not type login username: " + username);
			throw ex;
		}
	}
	
	
	/**
	 * <b>Author</b>: <br>
	 * <b>Date</b>: <br>
	 * <b>Description</b><br>
	 * 
	 * @param 
	 * @return 
	 */
	public void typePassword(String password) {
		Logging.info("Typing login password: " + password);
		String login_password_field = PropertyHelper.coreLoginFile
				.getPropertyValue("login_password_field");
		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(login_password_field))
					.sendKeys(password.trim());
		} catch (WebDriverException ex) {
			Logging.error("Could not type login password: " + password);
			throw ex;
		}
	}
	
	
	/**
	 * <b>Author</b>: <br>
	 * <b>Date</b>: <br>
	 * <b>Description</b><br>
	 * 
	 * @param 
	 * @return 
	 */
	public void clickLoginButton() {
        Logging.info("Clicking on Login Button");
		String login_login_button = PropertyHelper.coreLoginFile
                .getPropertyValue("login_login_button");
		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(login_login_button)).click();
			
			this.waitForMainPageToLoad(30);

			this.clickDisableOverlay(2);

			this.clickDisableOverlay(2);

			Logging.info("Home page is displayed after login.");
		} catch (WebDriverException ex) {
			Logging.error("Could not click on login button");
			throw ex;
		}
	}
	
	
	/**
	 * @Method Name : waitForLoginPageToLoad
	 * @author : Jerry Zhang
	 * @Created on : 09/15/2014
	 * @Description : This method is used to make the automation wait until the 
	 * 				Login page has completely loaded before executing next action 
	 * 				and then select default language to be English.
	 * @Parameter 1: int timeoutInSeconds - value specified in seconds to wait
	 */
	public void waitForLoginPageToLoad(int timeoutInSeconds) {
		Logging.info("Waiting for login page to load, timeout seconds: " + timeoutInSeconds);
		// Verify login page loaded then change language.
		boolean loginPageLoaded = this.verifyLoginPageLoaded(timeoutInSeconds);
		if (loginPageLoaded) {
			//this.selectLanguageAsEnglishOnLoginPage();
			// Verify login page loaded again after language changing.
			this.verifyLoginPageLoaded(timeoutInSeconds);
		}
	}
	
	
	/**
	 * @Method Name : verifyLoginPageLoaded
	 * @author : Jerry Zhang
	 * @Created on : 09/15/2014
	 * @Description : This method will verify whether login page loaded correctly.
	 * @Parameter 1: int timeoutInSeconds - value specified in seconds to wait
	 * @Return: boolean - true: page loaded / false: page not loaded
	 */
	public boolean verifyLoginPageLoaded(int timeoutInSeconds) {
		
		boolean result = false;
		String login_username_field = PropertyHelper.coreLoginFile
				.getPropertyValue("login_username_field");
		String login_password_field = PropertyHelper.coreLoginFile
				.getPropertyValue("login_password_field");
		String login_login_button = PropertyHelper.coreLoginFile
				.getPropertyValue("login_login_button");
		String login_language_box = PropertyHelper.coreLoginFile
				.getPropertyValue("login_language_box");
		
		try {
			boolean flag1 = Tools.waitUntilElementDisplayedByXpath(login_username_field, 
					timeoutInSeconds);
			if (!flag1) { throw new NoSuchElementException(""); }
			
			boolean flag2 = Tools.waitUntilElementDisplayedByXpath(login_password_field, 
					timeoutInSeconds);
			if (!flag2) { throw new NoSuchElementException(""); }
			
			boolean flag3 = Tools.waitUntilElementDisplayedByXpath(login_login_button, 
					timeoutInSeconds);
			if (!flag3) { throw new NoSuchElementException(""); }
			
			boolean flag4 = Tools.waitUntilElementDisplayedByXpath(login_language_box, 
					timeoutInSeconds);
			if (!flag4) { throw new NoSuchElementException(""); }
			
			// All core elements loaded.
			result = true;
		} catch (WebDriverException ex) {
			Logging.warn("Login page could not be confirmed as loaded");
		}
		
		if (result) {
			Logging.info("Login page loaded");
		}
		return result;
	}
	
	
	/**
	 * @Method Name : selectLanguageAsEnglishOnLoginPage
	 * @author : Jerry Zhang
	 * @Created on : 09/15/2014
	 * @Description : This method will select language as English on login page.
	 */
	public void selectLanguageAsEnglishOnLoginPage() {
		
		Logging.info("Selecting language as English on login page");
		String login_language_box = PropertyHelper.coreLoginFile
				.getPropertyValue("login_language_box");
		String login_language_selected = PropertyHelper.coreLoginFile
				.getPropertyValue("login_language_selected");
		String login_language_option_english = PropertyHelper.coreLoginFile
				.getPropertyValue("login_language_option_english");
		
		try {
			Base.base.getDriver().switchTo().defaultContent();
	    	Base.base.getDriver().manage().timeouts()
	        		.implicitlyWait(Long.parseLong(Base.timeout), TimeUnit.SECONDS);
	    	
	    	Base.base.getDriver().findElement(By.xpath(login_language_box)).click();
	    	
	    	/*if (!Base.base.getDriver().findElement(By.xpath(login_language_selected))
	    			.getText().contains("English")) {
	    		Base.base.getDriver().findElement(By.xpath(login_language_option_english)).click();
	    	} else {
	    		// English is already selected, click on language box to dismiss the menu.
	    		Base.base.getDriver().findElement(By.xpath(login_language_box)).click();
	    	}
	    	*/
			Base.base.getDriver().findElement(By.xpath(login_language_option_english)).click();
	    	
		} catch (WebDriverException ex) {
			Logging.error("Could not select language English on login page");
		}
	}
	
	
	/**
	 * @Method_Name : waitForMainPageToLoad
	 * @author : Jerry Zhang
	 * @Created_Date : 03/14/2014
	 * @Description : This method is used to make the automation wait until the
	 * 				Main page has completely loaded before executing next action.
	 * @Parameter_1: int timeoutInSeconds - value specified in seconds to wait
	 */
	public boolean waitForMainPageToLoad(int timeoutInSeconds) {
		Logging.info("Waiting for main page to load, timeout seconds: " + timeoutInSeconds);
		boolean result = false;
		String login_mainpage_navigation_tabs = PropertyHelper.coreLoginFile
				.getPropertyValue("login_mainpage_navigation_tabs");
		String login_mainpage_inbox = PropertyHelper.coreLoginFile
				.getPropertyValue("login_mainpage_inbox");
		String login_mainpage_mail_toolbar = PropertyHelper.coreLoginFile
				.getPropertyValue("login_mainpage_mail_toolbar");
		
		try {
			boolean flag1 = Tools.waitUntilElementDisplayedByXpath(login_mainpage_navigation_tabs, 
					timeoutInSeconds);
			if (!flag1) { throw new NoSuchElementException(""); }
			
			boolean flag2 = Tools.waitUntilElementDisplayedByXpath(login_mainpage_inbox, 
					timeoutInSeconds);
			if (!flag2) { throw new NoSuchElementException(""); }
			
			boolean flag3 = Tools.waitUntilElementDisplayedByXpath(login_mainpage_mail_toolbar, 
					timeoutInSeconds);
			if (!flag3) { throw new NoSuchElementException(""); }
			// All core elements loaded.
			result = true;
			
			this.waitForLoadMaskDismissed();
			
			
		} catch (WebDriverException ex) {
			Logging.warn("Main page could not be confirmed as loaded");
		}
		
		if (result) {
	        Logging.info("Main page loaded ");
		}
        return result;
    }
	
  public boolean isErrorPopDisplayed() {
    Logging.info("check if the error of conversation view is displayed");
    String error_popup = PropertyHelper.coreLoginFile.getPropertyValue("error_popup");
    try {
      Tools.setDriverDefaultValues();
      List<WebElement> popupError = Base.base.getDriver().findElements(By.xpath(error_popup));
      if (popupError.size() == 0)
        return false;
      else
        return true;
    } catch (WebDriverException ex) {
      Logging.error("Could not click on login button");
      throw ex;
    }
  }

  /**
   * Check if use conversation view
   * 
   * @author Fiona Zhang
   */
  public boolean isConversionViewPattern() {
    String conversition_view_enable =
        PropertyHelper.coreLoginFile.getPropertyValue("conversition_view_enable");
    boolean isConversationViewEnable = false;
    try {
      Tools.setDriverDefaultValues();
      JavascriptExecutor js = (JavascriptExecutor) Base.base.getDriver();
      String result = (String) js.executeScript(conversition_view_enable);
      if (result.equals("true"))
        isConversationViewEnable = true;
      Logging.info("Is use conversation-view : " + isConversationViewEnable);
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }
    return true;
  }
  
  
	/**
	 * @Method Name : verifyOverlayDisplay
	 * @author : Joey Fang
	 * @Created on : 12/27/2017
	 * @Description : This method will verify whether overlay page display.
	 * @Parameter : int timeoutInSeconds - value specified in seconds to wait
	 * @Return: boolean - true: page loaded / false: page not loaded
	 */
	public boolean verifyOverlayDisplay(int timeoutInSeconds) {

		boolean result = false;
		String overlayRadioButton = PropertyHelper.coreLoginFile.getPropertyValue("overlayRadioButton");
		

		try {
			
			boolean flag1 = Tools.waitUntilElementDisplayedByXpath(overlayRadioButton, timeoutInSeconds);
			
			if (flag1) {
				result = true;
				Logging.info("Overlay shows");
			}
			
		} catch (WebDriverException ex) {
			Logging.warn("Overlay does not show");
		}

		return result;
	}
	
  
	/**
	 * @Method Name : clickDisableOverlay
	 * @author : Joey Fang
	 * @Created on : 12/27/2017
	 * @Description : This method is used to disable the overlay when first login
	 * @Parameter
	 */
	public void clickDisableOverlay(int timeoutInSeconds) {
		// Verify login page loaded then change language.
		boolean overlayDisplay = this.verifyOverlayDisplay(timeoutInSeconds);
		try {
			if (overlayDisplay) {
				Logging.info("Clicking the Don't show overlay again");
				String overlayRadioButtonShow = PropertyHelper.coreLoginFile.getPropertyValue("overlayRadioButtonShow");
				String overlayOKButton = PropertyHelper.coreLoginFile.getPropertyValue("overlayOKButton");
				Base.base.getDriver().findElement(By.xpath(overlayRadioButtonShow)).click();
				Base.base.getDriver().findElement(By.xpath(overlayOKButton)).click();
			}
			this.waitForMainPageToLoad(30);
		} catch (WebDriverException ex) {
			Logging.error("Could not click OK button on overlay page");
			throw ex;
		}

	}
}