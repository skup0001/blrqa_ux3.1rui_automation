package com.synchronoss.pageobject;

import java.awt.Color;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebDriver.Navigation;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import com.jcraft.jsch.Logger;
import com.synchronoss.core.Base;
import com.synchronoss.core.Logging;
import com.synchronoss.core.PropertyHelper;
import com.synchronoss.core.SeleniumGrid;
import com.synchronoss.core.Tools;

/**
 * Super class of all page object classes. All common properties and methods
 * should be defined in this class.
 */
public class PageObjectBase {

	// #########################################################################
	// Static Constants
	// #########################################################################

	// Definitions for timeout value that will be used mainly in page
	// objects class for waiting actions.
	protected final int timeoutShort = Base.timeoutShort;
	protected final int timeoutMedium = Base.timeoutMedium;
	protected final int timeoutLong = Base.timeoutLong;

	String currentHandle = null ;

	// Button names
	public static final String YES = "Yes";
	public static final String NO = "No";
	public static final String OK = "OK";
	public static final String SAVE = "Save";
	public static final String CANCEL = "Cancel";
	public static final String DELETE = "Delete";
	
	//emoji group
    public static final String EMOJIGROUP_RECENT = "Recent";
    public static final String EMOJIGROUP_FACE = "Face";
    public static final String EMOJIGROUP_FEELING = "Feeling";
    public static final String EMOJIGROUP_SEASON = "Season";
    public static final String EMOJIGROUP_CHARACTER = "Character";
    public static final String EMOJIGROUP_FOOD = "Food";
    public static final String EMOJIGROUP_LOFE = "Life";
    public static final String EMOJIGROUP_TOOL = "Tool";
    public static final String EMOJIGROUP_HOOBY = "Hobby";
    public static final String EMOJIGROUP_SYNMBOLS = "Symbols";
	// Definition for emoji value
	public static final String emoji_263a = "sprite U263A";
	public static final String emoji_263a_preview = "sprite U-_263a";
	// Time zone cities
	public static final String AMERICA_NEWYORK = "America/New York";
	public static final String ASIA_SHANGHAI = "Asia/Shanghai";
	public static final String EUROPE_DUBLIN = "Europe/Dublin";

	// Date-Time format representations in Settings
	public static final String DATE_FORMAT_1 = "DD/MM/YYYY";
	public static final String DATE_FORMAT_2 = "DD/MM/YY";
	public static final String DATE_FORMAT_3 = "MM/DD/YYYY";
	public static final String DATE_FORMAT_4 = "YYYY/MM/DD";
	public static final String DATE_FORMAT_5 = "DD-MM-YYYY";
	public static final String DATE_FORMAT_6 = "DD.MM.YYYY";
	public static final String TIME_FORMAT_12_HOUR = "12-Hour";
	public static final String TIME_FORMAT_24_HOUR = "24-Hour";

	// Date-Time patterns
	private static final String DATE_PATTERN_1 = "dd/MM/yyyy";
	private static final String DATE_PATTERN_2 = "dd/MM/yy";
	private static final String DATE_PATTERN_3 = "MM/dd/yyyy";
	private static final String DATE_PATTERN_4 = "yyyy/MM/dd";
	private static final String DATE_PATTERN_5 = "dd-MM-yyyy";
	private static final String DATE_PATTERN_6 = "dd.MM.yyyy";
	private static final String TIME_PATTERN_12_HOUR = "h:mm a";
	private static final String TIME_PATTERN_24_HOUR = "HH:mm";

	// Static HashMap that holds Date-Time format-pattern mappings
	protected static HashMap<String, String> formatPatternMappings = null;

	/*
	 * This class should not be instantiated explicitly.
	 */
	protected PageObjectBase() {

		if (formatPatternMappings == null) {
			formatPatternMappings = new HashMap<String, String>();
			formatPatternMappings.put(DATE_FORMAT_1, DATE_PATTERN_1);
			formatPatternMappings.put(DATE_FORMAT_2, DATE_PATTERN_2);
			formatPatternMappings.put(DATE_FORMAT_3, DATE_PATTERN_3);
			formatPatternMappings.put(DATE_FORMAT_4, DATE_PATTERN_4);
			formatPatternMappings.put(DATE_FORMAT_5, DATE_PATTERN_5);
			formatPatternMappings.put(DATE_FORMAT_6, DATE_PATTERN_6);
			formatPatternMappings.put(TIME_FORMAT_12_HOUR, TIME_PATTERN_12_HOUR);
			formatPatternMappings.put(TIME_FORMAT_24_HOUR, TIME_PATTERN_24_HOUR);
		}
	}

	/**
	 * @Method_Name: waitForLoadMaskDismissed
	 * @Author: Jerry Zhang
	 * @Created_Date: 03/17/2014
	 * @Description: Waits for any load mask dismissed.
	 */
	public void waitForLoadMaskDismissed() {
		String common_load_mask = PropertyHelper.pageObjectBaseFile.getPropertyValue("common_load_mask");
		try {
			Tools.setDriverDefaultValues();
			Tools.waitUntilElementNotDisplayedByXpath(common_load_mask, this.timeoutLong);
		} catch (WebDriverException ex) {
			Logging.warn("Fail to wait for load mask dismissed");
		}
	}

	/**
	 * <b>Author</b>: Jerry Zhang<br>
	 * <b>Date</b>: Jun 08, 2015<br>
	 * <b>Description</b><br>
	 * Clicks button on message box popup. One of the following options should
	 * be supplied: - Yes - No - Cancel
	 * 
	 * @param button
	 *            button name
	 * @return void
	 */
	public void clickButtonOnMessageBox(String button) {
		Logging.info("Clicking on message box button: " + button);
		String common_msgbox_button = PropertyHelper.pageObjectBaseFile.getPropertyValue("common_msgbox_button")
				.replace("+variable+", button);

		try {
			//Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(common_msgbox_button)).click();
			this.waitForLoadMaskDismissed();
		} catch (NoSuchElementException ex) {
			Logging.error("Could not click on message box button: " + button);
			throw ex;
		}
	}
	
  /**
   * <b>Author</b>: Vivian Xie<br>
   * <b>Date</b>: May 18, 2017<br>
   * <b>Description</b><br>
   * Clicks button on message box popup. One of the following options should be supplied: - Yes - No
   * - Cancel
   * 
   * @param button button name
   * @param ifWaitForLoadMaskDimiss wait for load mask dissmiss if the value is true
   * @return void
   */
  public void clickButtonOnMessageBox(String button, boolean ifWaitForLoadMaskDimiss) {
    Logging.info("Clicking on message box button: " + button);
    String common_msgbox_button = PropertyHelper.pageObjectBaseFile
        .getPropertyValue("common_msgbox_button").replace("+variable+", button);

    try {
      // Tools.setDriverDefaultValues();
      Base.base.getDriver().findElement(By.xpath(common_msgbox_button)).click();
      if (ifWaitForLoadMaskDimiss == true)
        this.waitForLoadMaskDismissed();
    } catch (NoSuchElementException ex) {
      Logging.error("Could not click on message box button: " + button);
      throw ex;
    }
  }

	/**
	 * <b>Author</b>: Jerry Zhang<br>
	 * <b>Date</b>: Apr 14, 2016<br>
	 * <b>Description</b><br>
	 * Clicks button on message box if it popped-up; otherwise ignored. One of
	 * the following options should be supplied: - Yes - No - Cancel
	 * 
	 * @param button
	 *            button name
	 * @return void
	 */
	public void clickButtonOnMessageBoxIfDisplayed(String button) {
		try {
			this.clickButtonOnMessageBox(button);
		} catch (WebDriverException ex) {
			Logging.info("Message box may not be displayed");
		}
	}

	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 06, 2016<br>
	 * <b>Description: </b><br>
	 * Verifies if the body content of a message box is as expected.
	 */
	public boolean verifyMessageBoxBody(String body) {
		Logging.info("Verifying if message box body is: " + body);

		String common_msgbox_body = PropertyHelper.pageObjectBaseFile.getPropertyValue("common_msgbox_body");
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			Tools.waitFor(1, TimeUnit.SECONDS);
			Logging.info(common_msgbox_body);
			String actText = Base.base.getDriver().findElement(By.xpath(common_msgbox_body)).getAttribute("innerHTML");
			Logging.info("Actual text of message box body: " + actText);

			if (body.equalsIgnoreCase(actText)) {
				result = true;
			}

			Logging.info("Message box body is as expected: " + result);
		} catch (WebDriverException ex) {
			Logging.error("Could not verify message box body");
			throw ex;
		}
		return result;
	}
	
	

	/**
	 * @Method_Name: waitForQuicktipsDismissed
	 * @Author: Jerry Zhang
	 * @Created_Date: Apr 13, 2016
	 * @Description: Waits for the specified quick tips bar dismissed which
	 *               contains text specified.
	 */
	public void waitForQuicktipsDismissed(String text) {
		String common_quicktips = PropertyHelper.pageObjectBaseFile.getPropertyValue("common_quicktips")
				.replace("+variable+", text);
		try {
			Tools.setDriverDefaultValues();
			Tools.waitUntilElementNotDisplayedByXpath(common_quicktips, this.timeoutLong);
		} catch (WebDriverException ex) {
			Logging.warn("Fail to wait for quick tips bar dismissed, text: " + text);
		}
	}

	/**
	 * @Method_Name: waitForQuicktipsDismissed
	 * @Author: Jerry Zhang
	 * @Created_Date: May 10, 2016
	 * @Description: Waits for any quick tips bar to be dismissed.
	 */
	public void waitForQuicktipsDismissed() {
		String common_quicktips_general = PropertyHelper.pageObjectBaseFile
				.getPropertyValue("common_quicktips_general");
		try {
			Tools.setDriverDefaultValues();
			Tools.waitUntilElementNotDisplayedByXpath(common_quicktips_general, this.timeoutLong);
		} catch (WebDriverException ex) {
			Logging.warn("Fail to wait for quick tips bar dismissed");
		}
	}

	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 13, 2016<br>
	 * <b>Description: </b><br>
	 * Selects the specified date from a date picker. This is the reusable
	 * version of date picker manipulation method, which can be used for
	 * Calendar and Tasks date pickers. <br>
	 * Note: a base path locator should be provided and it must be unique since
	 * sub-elements locating relies on this base path.
	 * 
	 * @param basePath
	 *            - a unique xpath locator that locates the whole outside box of
	 *            the date picker.
	 */
	public void selectInDatePicker(LocalDate date, String basePath) {

		int year = date.getYear();
		String month = date.getMonth().getDisplayName(TextStyle.SHORT, Locale.ENGLISH);
		int day = date.getDayOfMonth();

		// Opens month picker
		String common_datepicker_split_button = basePath
				+ PropertyHelper.pageObjectBaseFile.getPropertyValue("common_datepicker_split_button");

		Base.base.getDriver().findElement(By.xpath(common_datepicker_split_button)).click();
		Tools.waitFor(1, TimeUnit.SECONDS);

		// Determines whether year-navigation required or not
		String common_datePicker_first_year_displayed = "(" + basePath
				+ PropertyHelper.pageObjectBaseFile.getPropertyValue("common_datePicker_first_year_displayed") + ")[1]";

		String yearNavButton = null;

		int firstYear = Integer
				.valueOf(Base.base.getDriver().findElement(By.xpath(common_datePicker_first_year_displayed)).getText());

		int naviCount = this.calculateYearNavigationInDatePicker(year, firstYear);

		if (naviCount < 0) {
			yearNavButton = basePath
					+ PropertyHelper.pageObjectBaseFile.getPropertyValue("common_datepicker_year_nav_prev");
		} else {
			yearNavButton = basePath
					+ PropertyHelper.pageObjectBaseFile.getPropertyValue("common_datepicker_year_nav_next");
		}

		for (int i = 0; i < Math.abs(naviCount); i++) {
			Base.base.getDriver().findElement(By.xpath(yearNavButton)).click();
			Tools.waitFor(500, TimeUnit.MILLISECONDS);
		}

		// Selects year
		String common_datePicker_monthpicker_year = basePath + PropertyHelper.pageObjectBaseFile
				.getPropertyValue("common_datePicker_monthpicker_year").replace("+variable+", String.valueOf(year));

		Base.base.getDriver().findElement(By.xpath(common_datePicker_monthpicker_year)).click();
		Tools.waitFor(1, TimeUnit.SECONDS);

		// Selects month
		String common_datePicker_monthpicker_month = basePath + PropertyHelper.pageObjectBaseFile
				.getPropertyValue("common_datePicker_monthpicker_month").replace("+variable+", month);
		Base.base.getDriver().findElement(By.xpath(common_datePicker_monthpicker_month)).click();
		Tools.waitFor(1, TimeUnit.SECONDS);

		// Clicks OK in month picker
		String common_datePicker_monthpicker_toolbar_button = basePath + PropertyHelper.pageObjectBaseFile
				.getPropertyValue("common_datePicker_monthpicker_toolbar_button").replace("+variable+", "OK");
		Base.base.getDriver().findElement(By.xpath(common_datePicker_monthpicker_toolbar_button)).click();
		Tools.waitFor(1, TimeUnit.SECONDS);

		// Selects day
		String common_datePicker_day = basePath + PropertyHelper.pageObjectBaseFile
				.getPropertyValue("common_datePicker_day").replace("+variable+", String.valueOf(day));
		Base.base.getDriver().findElement(By.xpath(common_datePicker_day)).click();
		for (int i = 0; i <= 3; i++) {
			if (Base.base.getDriver().findElement(By.xpath(common_datePicker_day)).isDisplayed()) {

				Base.base.getDriver().findElement(By.xpath(common_datePicker_day)).click();
				Tools.waitFor(1, TimeUnit.SECONDS);

			}
		}

		Tools.waitFor(5, TimeUnit.SECONDS);
	}

	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jul 09, 2016<br>
	 * <b>Description: </b><br>
	 * Selects the specified date from a floating date picker. Applicable to all
	 * floating date pickers in Calendar and Tasks. Not applicable to fixed date
	 * pickers that are located at the left pane.
	 */
	public void selectDateInFloatingDatePicker(LocalDate date) {
		Logging.info("Selecting date in floating date picker: " + date);
		String common_floating_datepicker = PropertyHelper.pageObjectBaseFile
				.getPropertyValue("common_floating_datepicker");
		try {

			this.selectInDatePicker(date, common_floating_datepicker);
		} catch (WebDriverException ex) {
			Logging.warn("Could not select date in floating date picker: " + date);
		}
	}

	/*
	 * Utility method used by SelectInDatePicker()
	 */
	private int calculateYearNavigationInDatePicker(int currentYear, int firstYear) {
		int count = 0;

		if ((firstYear - currentYear) > 0) {
			// go to previous pages
			count = ((firstYear - currentYear - 1) / 10) + 1;
			return 0 - count;
		} else {
			// go to next pages
			count = (currentYear - firstYear) / 10;
			return count;
		}
	}

	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 23, 2016<br>
	 * <b>Description: </b><br>
	 * Gets formatted local date String representation.
	 */
	public String getFormattedLocalDateString(LocalDate localDate, String dateFormat) {
		return localDate.format(DateTimeFormatter.ofPattern(formatPatternMappings.get(dateFormat)));
	}

	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 23, 2016<br>
	 * <b>Description: </b><br>
	 * Gets formatted local time String representation.
	 */
	public String getFormattedLocalTimeString(LocalTime localTime, String timeFormat) {
		return localTime.format(DateTimeFormatter.ofPattern(formatPatternMappings.get(timeFormat)));
	}

	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 18, 2016<br>
	 * <b>Description: </b><br>
	 * Gets Webtop settings preference by executing a JavaScript code snippet.
	 */
	public String getPreferenceByJs(String js) {
		String jsCode = PropertyHelper.pageObjectBaseFile.getPropertyValue(js);

		JavascriptExecutor jsExecutor = (JavascriptExecutor) Base.base.getDriver();
		return (String) jsExecutor.executeScript(jsCode);
	}

	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 19, 2016<br>
	 * <b>Description: </b><br>
	 * Gets Settings preference - Use Device Time Zone.
	 */
	public Boolean getPreference_UseDeviceTimeZone() {
		Boolean useDeviceTZ = Boolean.parseBoolean(this.getPreferenceByJs("js_use_device_timezone"));
		Logging.info("Settings preference - Use Device Time Zone: " + useDeviceTZ);
		return useDeviceTZ;
	}

	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 20, 2016<br>
	 * <b>Description: </b><br>
	 * Gets Settings preference - Local City.
	 */
	public String getPreference_LocalCity() {
		String city = this.getPreferenceByJs("js_local_city");
		Logging.info("Settings preference - Local City: " + city);
		return city;
	}

	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 19, 2016<br>
	 * <b>Description: </b><br>
	 * Gets Settings preference - Date Format.
	 */
	public String getPreference_DateFormat() {
		String format = this.getPreferenceByJs("js_date_format");

		if (formatPatternMappings.containsKey(format)) {
			Logging.info("Settings preference - Date Format: " + format);
		} else {
			Logging.warn("Mapping not found: " + "Settings preference - Date Format: " + format);
		}

		return format;
	}

	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 19, 2016<br>
	 * <b>Description: </b><br>
	 * Gets Settings preference - Time Format
	 */
	public String getPreference_TimeFormat() {
		Boolean is24Hour = Boolean.parseBoolean(this.getPreferenceByJs("js_time_format"));

		String format;
		if (is24Hour) {
			format = TIME_FORMAT_24_HOUR;
		} else {
			format = TIME_FORMAT_12_HOUR;
		}
		Logging.info("Settings preference - Time Format: " + format);
		return format;
	}

	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jul 16, 2016<br>
	 * <b>Description: </b><br>
	 * Used by page objects to check or uncheck a checkbox. Note: this method
	 * can not be sued by 'checker' elements such as email item checkers
	 * preceding to an email message.
	 */
	protected void checkOrUncheck(String checkboxField, String checkboxXpath, Boolean check) {
		Logging.info((check ? "Checking" : "Unchecking") + " checkbox field: " + checkboxField);

		try {
			Tools.setDriverDefaultValues();
			this.waitForLoadMaskDismissed();

			// Checkbox is enclosed in a 'span' element, with its enclosing
			// table element has the following
			// css class pattern.
			String ancestorTable = checkboxXpath + "//ancestor::table[contains(@class,'type-checkbox')]";

			boolean isEnabled = Base.base.getDriver().findElement(By.xpath(ancestorTable)).getAttribute("class")
					.contains("cb-checked");

			if ((check && !isEnabled) || (!check && isEnabled)) {
				Base.base.getDriver().findElement(By.xpath(checkboxXpath)).click();
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not " + (check ? "Check" : "Uncheck") + " checkbox field: " + checkboxField);
			throw ex;
		}
	}

	/**
	 * @Method_Name: refresh the page
	 * @Author: Vivian Xie
	 * @Created_Date: Sept 19, 2016
	 * @Description: Prints debug information about element's inner HTML
	 *               structure.
	 */
	public void refresh() {
		try {
			Base.base.getDriver().navigate().refresh();
			Thread.sleep(2000);
			Alert alert = Base.base.getDriver().switchTo().alert();
			alert.accept();

		} catch (WebDriverException ex) {
			Logging.warn("Could not refresh the page");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// #########################################################################
	// Debugging Methods
	// #########################################################################

	/**
	 * @Method_Name: debugGetElementInnerHtml
	 * @Author: Jerry Zhang
	 * @Created_Date: Apr 23, 2016
	 * @Description: Prints debug information about element's inner HTML
	 *               structure.
	 */
	public void debugGetElementInnerHtml(String element) {
		try {
			String innerHtml = Base.base.getDriver().findElement(By.xpath(element)).getAttribute("innerHTML");
			Logging.debug("debugGetElementInnerHtml -> Element XPath: " + element);
			Logging.debug("debugGetElementInnerHtml -> Element inner HTML: " + innerHtml);
		} catch (WebDriverException ex) {
			Logging.warn("Fail to get element inner HTML: " + element);
		}
	}
	
	/**
     * @Method_Name: ColorToHexEncoding
     * @Author: Vivian Xie
     * @Created_Date: Dec 2, 2016
     * @Description: Convert color to hex String.
     * @return: return hex String of this color
     */
	 public static String ColorToHexEncoding(Color color) {
       String R, G, B;
       StringBuffer sb = new StringBuffer();

       R = Integer.toHexString(color.getRed());
       G = Integer.toHexString(color.getGreen());
       B = Integer.toHexString(color.getBlue());
       
       R = R.length() == 1 ? "0" + R : R;
       G = G.length() == 1 ? "0" + G : G;
       B = B.length() == 1 ? "0" + B : B;


       sb.append("#");
       sb.append(R);
       sb.append(G);
       sb.append(B);

       return sb.toString();
   }

	  /**
	   * <b>Author</b>: Vivian Xie <br>
	   * <b>Date</b>: Nov 21, 2016 <br>
	   * <b>Description</b><br>
	   * run java script to get browser datetime <b>Parameters</b><br>
	   * String type - value can be Date, Day,Days,FullYear,Hours,Milliseconds,Minutes,Month,Seconds,
	   * Time,TimeZoneOffset,UTCDate,UTCDay,UTCFullYear,UTCHours,
	   * UTCMilliseconds,UTCMinutes,UTCMonth,UTCSecond,Year 
	   * <b>return:</b> return the value of the
	   * specific type
	   */
	  public int getBrowserDateTime(String type) {
	    Logging.info("Starting to get brower time:" +type );
	    JavascriptExecutor jsExecutor = (JavascriptExecutor) Base.base.getDriver();
	    Long value = null;
	    String jsCode = " var datetime = new Date(); var wantedPart = datetime.get" + type
	        + "(); return wantedPart";
	    try {
	      Logging.info("object "+jsExecutor.executeScript(jsCode).getClass());
//	      Logging.info("object "+(String)jsExecutor.executeScript(jsCode));

	      value = (Long) jsExecutor.executeScript(jsCode);
	    } catch (Exception e) {
	      Logging.info("Failed to run jave script:"+ jsCode);
	      e.printStackTrace();
	    }
	    return value.intValue();

	  }
	  
  /**
   * <b>Author</b>: Vivian Xie <br>
   * <b>Date</b>: Dec 14, 2016 <br>
   * <b>Description</b><br>
   * run java script to get browser datetime <b>return:</b> return the browser's datetime specific
   * type
   */
  public LocalDateTime getBrowserDateTime() {
    Logging.info("Starting to get brower datetime");
    JavascriptExecutor jsExecutor = (JavascriptExecutor) Base.base.getDriver();
    LocalDateTime value = null;
    String jsCode = " var time = new Date();  var y = time.getFullYear();"
        + " var m = time.getMonth()+1; m=m<10?'0'+m:m;" + " var d = time.getDate();d=d<10?'0'+d:d;"
        + " var h = time.getHours();h=h<10?'0'+h:h;"
        + " var mm = time.getMinutes();mm=mm<10?'0'+mm:mm;"
        + " var s = time.getSeconds();s=s<10?'0'+s:s;return y+'-'+m+'-'+d+' '+h+':'+mm+':'+s;";
    String strValue = null;
    try {
      Logging.info("object " + jsExecutor.executeScript(jsCode).getClass());
      // Logging.info("object "+(String)jsExecutor.executeScript(jsCode));
      strValue = (String) jsExecutor.executeScript(jsCode);
      Logging.info(strValue);
      DateTimeFormatter formatter =
          DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
      value = LocalDateTime.parse(strValue, formatter);

    } catch (Exception e) {
      Logging.info("Failed to run jave script:" + jsCode);
      e.printStackTrace();
    }
    return value;

  }
  
  /**
   * <b>Author</b>: Vivian Xie<br>
   * <b>Date</b>: Dec 14, 2016<br>
   * <b>Description</b><br>
   * Select Editor format</b><br>
   * String format - value can be "Rich" and "Plain"
   */
  public void selectEditorFormat(String format) {
    Logging.info("Starting to  select editor format  " + format);

    String email_composer_format_combbox =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_format_combbox");
    String email_composer_format_options = PropertyHelper.coreEmailComposer
        .getPropertyValue("email_composer_format_options").replace("+variable+", format);

    try {
      Base.base.getDriver().findElement(By.xpath(email_composer_format_combbox)).click();
      Tools.waitUntilElementDisplayedByXpath(email_composer_format_options, 1);
      Base.base.getDriver().findElement(By.xpath(email_composer_format_options)).click();
    } catch (WebDriverException ex) {
      Logging.error("Could not select format" + format);
      throw ex;
    }
  }
  
  /**
   * @Method_Name: CreateNewTab
   * @Author: Vivian Xie
   * @Created_Date: Feb 7, 2016
   * @Description: Create a new tab in browser.
   * @param: url  -- open the url in new tab
   */
  public void createNewTab(String url) {
    Logging.info("Starting to  create a new tab: " + url);
    try { 
      JavascriptExecutor oJavaScriptExecutor = (JavascriptExecutor)Base.base.getDriver();  
      oJavaScriptExecutor.executeScript("window.open()");  
      switchToWindow("Untitled");
      System.out.println( Base.base.getDriver().getTitle());
      Navigation nav = Base.base.getDriver().navigate();
      nav.to(url);  
    } catch (WebDriverException ex) {
      Logging.error("error when opening new tab");
      throw ex;
    }
  }
 
  /**
   * switch to the specific window
   * 
   * @Author: Vivian Xie
   * @Created_Date: Feb 7, 2016
   * @param: windowTitle -- the windows title.
   */
  public  void switchToWindow(String windowTitle) {
    try {
      Logging.info("Starting to switch to window " + windowTitle);
      WebDriver dr = Base.base.getDriver();
      // Put all the windows handle to set
      // Declaring the current window handle as a global variable for switching back to the main window when the child window is closed.
		currentHandle = dr.getWindowHandle();
      Logging.info("Current Window Title : " + dr.getTitle());
      Logging.info("Current Window : " + currentHandle);
      Set<String> handles = dr.getWindowHandles();
      for (String s : handles) {
        if (s.equals(currentHandle))
        { Logging.info("equal");
          continue;
        }
        else {
          dr.switchTo().window(s);
          Logging.info(dr.getTitle());
          if (dr.getTitle().contains(windowTitle)) {
            dr.manage().window().maximize();
            System.out.println("Switch to window: " + windowTitle + " successfully!");
            break;
          }
        }
      }
    } catch (Exception e) {
      System.out.printf("Window: " + windowTitle + " cound not found!", e.fillInStackTrace());
    }
  }

	public void switchToMainWindow(){
		Logging.info("Switching back to the main window.");
		WebDriver dr = Base.base.getDriver();
		dr.switchTo().window(currentHandle);
	}
  
  
  /**
   * get windows number
   * 
   * @Author Vivian Xie
   * @Created May/17th/2017
   */
  public int getWindowsNumber() {
    Logging.info("Starting to get windows total number ");
    Set<String> handles = null;
    int result = 0;
    try {
      handles = Base.base.getDriver().getWindowHandles();
    } catch (Exception ex) {
      System.out.printf("Could not get windows total number");
      throw ex;
    }  
    result = handles.size();
    Logging.info("Total windows number is "+ result);
    return result;
  }
  
 
  /**
   * click button on alert window
   * 
   * @param buttonName -- the button name to be clicked
   * @Author Vivian Xie
   * @Created May/17th/2017
   */
  public void clickButtonOnAlert(String buttonName)
  {
    Alert alert = Base.base.getDriver().switchTo().alert();
    if (buttonName.equalsIgnoreCase("accept"))
      alert.accept();
    else
    alert.dismiss();
    
    
  }
  
	/**
	 * <b>Author: </b>Charlie Li<br>
	 * <b>Date: </b>September 04, 2017<br>
	 * <b>Description: </b><br>
	 * Verifies if the title of a message is as expected.
	 */
	public boolean verifyToastMessage(String message) {
		Logging.info("Verifying if message is: " + message);

		String common_toast_msg = PropertyHelper.pageObjectBaseFile
				.getPropertyValue("common_toast_msg").replace("+variable+",
						message);
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			Tools.waitUntilElementDisplayedByXpath(common_toast_msg, 10);
			result = true;
			Logging.info(message);

		} catch (WebDriverException ex) {
			Logging.error("Could not verify message: " + message);
			throw ex;
		}
		return result;
	}

	/**
	 * <b>Author: </b>Charlie Li<br>
	 * <b>Date: </b>September 04, 2017<br>
	 * <b>Description: </b><br>
	 * Set WebtopConfig.ui.floatInfoDuration
	 */
	public void setFloatInfoDuration(String duration) {
		Logging.info("Set WebtopConfig.ui.floatInfoDuration = " + duration);

		JavascriptExecutor jsExecutor = (JavascriptExecutor) Base.base
				.getDriver();
		jsExecutor.executeScript("WebtopConfig.ui.floatInfoDuration = "
				+ duration);
	}
	

  /**
   * <b>Author</b>: Fiona Zhang<br>
   * <b>Date</b>: Sep 5, 2017<br>
   * <b>Description</b><br>
   * Get current pop up warning message
   * 
   */
  public String getWarningMessage() {
    Logging.info("Get current popup warnning message : ");
    String result = null;
    String pop_up_warning_message =
        PropertyHelper.pageObjectBaseFile.getPropertyValue("pop_up_warning_message");
    try {
      Tools.setDriverDefaultValues();
      result = Base.base.getDriver().findElement(By.xpath(pop_up_warning_message)).getText();
    } catch (WebDriverException ex) {
      Logging.error("Could not get current popup message");
      throw ex;
    }
    return result;
  }
}

