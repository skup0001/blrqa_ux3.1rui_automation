package com.synchronoss.pageobject;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import com.synchronoss.core.Base;
import com.synchronoss.core.Logging;
import com.synchronoss.core.PropertyHelper;
import com.synchronoss.core.Tools;

public class CoreSettings extends PageObjectBase {

	// Checkbox field final Strings. The values are UIA specific strings in
	// element's
	// css class name. Use this as xpath replacement part.
	public static final String CHKBOX_REPLY_QUOTING_ORIGINAL_MESSAGE = "automation-settings-general-replyquoting-field";
	public static final String CHKBOX_COMPOSE_MESSAGE_IN_RICH_FORMAT = "automation-settings-general-editor-field";
	public static final String CHKBOX_INCLUDE_SIGNATURE = "automation-settings-signature-autoinsert-field";
	public static final String CHKBOX_NEW_SIGNATURE_SET_AS_DEFAULT = "automation-settings-signaturewin-default-field";
	public static final String CHKBOX_ENABLE_AUTOREPLY = "automation-settings-autoreply-enabled-field";
	public static final String CHKBOX_QUOTE_ORIGINAL_EMAIL_IN_AUTOREPLY = "automation-settings-autoreply-original-field";

	// Constant names.
	public static final String AUTO_REPLY_START_DATE = "Auto-Reply Start Date";
	public static final String AUTO_REPLY_END_DATE = "Auto-Reply End Date";

	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jul 16, 2016<br>
	 * <b>Description: </b><br>
	 * Check or uncheck a settings checkbox field.
	 */
	public void clickOnCheckbox(String field, Boolean check) {
		String settings_checkbox = PropertyHelper.coreSettings.getPropertyValue("settings_checkbox")
				.replace("+variable+", field);
		this.checkOrUncheck(field, settings_checkbox, check);
	}

	/**
	 * @Method Name: checkMailGeneralSetting
	 * @Author: Nitin Tyagi
	 * @Created On: 08/06/2014
	 * @Description: This method will click on the checkbox under mail general
	 *               settings
	 * 
	 * @Parameter1: String item - setting name One of the following item should be
	 *              provided: 1 Automatically save my outgoing mail
	 *
	 * @Parameter2: True or False for check and uncheck
	 * 
	 */
	public void checkMailGeneralSetting(String item, boolean check) {
		Logging.info("Clicking on settings item: " + item);

		String settings_general_item_check = PropertyHelper.coreSettings.getPropertyValue("settings_general_item_check")
				.replace("+variable+", item);

		try {
			Tools.setDriverDefaultValues();
			Tools.scrollElementIntoViewByXpath(settings_general_item_check);

			boolean isChecked = Base.base.getDriver().findElement(By.xpath(settings_general_item_check))
					.getAttribute("class").contains("x-form-cb-checked");

			if (check) {

				if (!isChecked) {
					Base.base.getDriver().findElement(By.xpath(settings_general_item_check))
							.findElement(By.xpath(".//span")).click();
				}
				Logging.info("Checked on checkbox: " + item);
			} else {

				if (isChecked) {

					Base.base.getDriver().findElement(By.xpath(settings_general_item_check))
							.findElement(By.xpath(".//span")).click();
				}
				Logging.info("Unchecked on checkbox: " + item);
			}

		} catch (WebDriverException ex) {
			Logging.error("Could not change Mail General Settings - " + item);
			throw ex;
		}
	}

	/**
	 * @Method Name: clickToolbarButtonInSettingsDetail
	 * @Author: Jerry zhang
	 * @Created On: 12/12/2014
	 * @Description: This method will click on a toolbar button in settings detail
	 *               page.
	 * @Parameter1: String button - button to click.
	 */
	public void clickToolbarButtonInSettingsDetail(String button) {
		Logging.info("Clicking on settings toolbar button: " + button);

		String settings_detail_toolbar_button = PropertyHelper.coreSettings
				.getPropertyValue("settings_detail_toolbar_button").replace("+variable+", button);

		try {
			Tools.setDriverDefaultValues();

			boolean unclickable = Base.base.getDriver().findElement(By.xpath(settings_detail_toolbar_button))
					.getAttribute("class").contains("x-item-disabled");

			if (!unclickable) {
				Base.base.getDriver().findElement(By.xpath(settings_detail_toolbar_button)).click();
				this.waitForLoadMaskDismissed();
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not click on" + button + " button");
			throw ex;
		}
	}

	/**
	 * @Method Name: clickSettingsItem
	 * @Author: Jerry zhang
	 * @Created On: 07/25/2014
	 * @Description: This method will click on the specified settings item in the
	 *               list.
	 * @Parameter1: String item - setting name One of the following item should be
	 *              provided: - Mail - Profile - TODO add necessary ones according
	 *              to UI
	 */
	public void clickSettingsItem(String item) {
		Logging.info("Clicking on settings item: " + item);

		item = item.trim();
		String settings_item = PropertyHelper.coreSettings.getPropertyValue("settings_item").replace("+variable+",
				item);

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(settings_item)).click();
			this.waitForLoadMaskDismissed();

		} catch (WebDriverException ex) {
			Logging.error("Could not click on settings item: " + item);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Aug 25, 2015<br>
	 * <b>Description</b><br>
	 * clear field content in Profile tab <b>Parameters</b><br>
	 * String field: the input field - firstname - lastname
	 */
	public void clearFieldInProfile(String field) {
		Logging.info("clear Profile: " + field);

		String settings_profile_textfield = PropertyHelper.coreSettings.getPropertyValue("settings_profile_textfield")
				.replace("+variable+", field);

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(settings_profile_textfield)).clear();
		} catch (WebDriverException ex) {
			Logging.error("Could not clear the text: " + field);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Aug 25, 2015<br>
	 * <b>Description</b><br>
	 * input text in field in Profile tab <b>Parameters</b><br>
	 * String field: the input field - firstname - lastname String text: the input
	 * content
	 */
	public void typeInFieldInProfile(String field, String text) {
		Logging.info("type text into: " + field);

		String settings_profile_textfield = PropertyHelper.coreSettings.getPropertyValue("settings_profile_textfield")
				.replace("+variable+", field);

		try {
			Tools.setDriverDefaultValues();
			this.clearFieldInProfile(field);
			WebElement textField = Base.base.getDriver().findElement(By.xpath(settings_profile_textfield));
			textField.click();
			textField.sendKeys(text);
		} catch (WebDriverException ex) {
			Logging.error("Could not type text into: " + field);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Aug 28, 2015<br>
	 * <b>Description</b><br>
	 * select date format <b>Parameters</b><br>
	 * String dateFormat - select date type One of the following item should be
	 * provided: - YYYY/MM/DD - DD/MM/YY - MM/DD/YYYY - DD/MM/YYYY
	 */
	public void selectDateFormat(String dateFormat) {
		Logging.info("Clicking on dateformat button and select dateformat as: " + dateFormat);

		String settings_date_format_arrow_trigger = PropertyHelper.coreSettings
				.getPropertyValue("settings_date_format_arrow_trigger");
		String settings_common_boundlist_item = PropertyHelper.coreSettings
				.getPropertyValue("settings_common_boundlist_item").replace("+variable+", dateFormat);

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(settings_date_format_arrow_trigger)).click();
			Base.base.getDriver().findElement(By.xpath(settings_common_boundlist_item)).click();

		} catch (WebDriverException ex) {
			Logging.error("Could not select dateformat");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Aug 28, 2015<br>
	 * <b>Description</b><br>
	 * select time format <b>Parameters</b><br>
	 * String dateFormat - select date type One of the following item should be
	 * provided: - 12 - 24
	 */
	public void selectTimeFormat(String timeFormat) {
		Logging.info("Clicking on timeFormat trigger button to select timeformat as: " + timeFormat + " Hr.");

		String settings_time_format_trigger_button = PropertyHelper.coreSettings
				.getPropertyValue("settings_time_format_trigger_button");
		String settings_time_format_boundlist_item = null;

		// Using if-else statements to make code to be compatible with both timeFormat
		// Strings that either contains "-Hour" or not.
		if (timeFormat.toLowerCase().contains("hour")) {
			settings_time_format_boundlist_item = PropertyHelper.coreSettings
					.getPropertyValue("settings_time_format_boundlist_item").replace("+variable+-Hour", timeFormat);
		} else {
			settings_time_format_boundlist_item = PropertyHelper.coreSettings
					.getPropertyValue("settings_time_format_boundlist_item").replace("+variable+", timeFormat);
		}

		try {
			Tools.setDriverDefaultValues();
			// boolean isChecked = Base.base.getDriver()
			// .findElement(By.xpath(settings_time_format_boundlist_item))
			// .getAttribute("class").contains("boundlist-selected");
			// if (!isChecked) {
			Base.base.getDriver().findElement(By.xpath(settings_time_format_trigger_button)).click();
			Base.base.getDriver().findElement(By.xpath(settings_time_format_boundlist_item)).click();
			Logging.info("Select timeFormat as: " + timeFormat + " Hr.");
			// } else {
			// Logging.info("The timeformat: "+timeFormat+" Hr. has been selected");
			// }
		} catch (WebDriverException ex) {
			Logging.error("Could not change timeFormat Settings as: - " + timeFormat);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Aug 28, 2015<br>
	 * <b>Description</b><br>
	 * change TimeZone settings "UseMyDeviceTimeZone" <b>Parameters</b><br>
	 * boolean check - true: to check / false: to uncheck
	 */
	public void changeTimeZoneSettings_UseMyDeviceTimeZone(boolean check) {
		Logging.info("Changing Time Zone Settings - Use my device's time zone: " + check);

		String settings_timezone_checkbox_field = PropertyHelper.coreSettings
				.getPropertyValue("settings_timezone_checkbox_field");
		String settings_timezone_checkbox = PropertyHelper.coreSettings.getPropertyValue("settings_timezone_checkbox");

		try {
			Tools.setDriverDefaultValues();
			Tools.waitUntilElementDisplayedByXpath(settings_timezone_checkbox_field, this.timeoutLong);

			boolean isChecked = Base.base.getDriver().findElement(By.xpath(settings_timezone_checkbox_field))
					.getAttribute("class").contains("cb-checked");

			if ((check && !isChecked) || (!check && isChecked)) {
				Base.base.getDriver().findElement(By.xpath(settings_timezone_checkbox)).click();
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not change Time Zone Settings - Use my device's time zone");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Aug 28, 2015<br>
	 * <b>Description</b><br>
	 * select date format <b>Parameters</b><br>
	 * String dateFormat - select date type One of the following item should be
	 * provided: - Asia/Shanghai - America/Los Angeles - Europe/Dublin - ...
	 */
	public void selectTimeZone(String timeZone) {
		Logging.info("Clicking on timezone trigger button and select timezone as: " + timeZone);

		String settings_timezone_arrow_trigger = PropertyHelper.coreSettings
				.getPropertyValue("settings_timezone_arrow_trigger");
		String settings_common_boundlist_item = PropertyHelper.coreSettings
				.getPropertyValue("settings_common_boundlist_item").replace("+variable+", timeZone);

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(settings_timezone_arrow_trigger)).click();
			this.waitForLoadMaskDismissed();
			Tools.scrollElementIntoViewByXpath(settings_common_boundlist_item);
			Base.base.getDriver().findElement(By.xpath(settings_common_boundlist_item)).click();

		} catch (WebDriverException ex) {
			Logging.error("Could not select timezone");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 2, 2015<br>
	 * <b>Description</b><br>
	 * select time period for save draft <b>Parameters</b><br>
	 * String timePeriod - time period One of the following item should be provided:
	 * - every minute - every 2 minutes - every 5 minutes
	 */
	public void selectTimePeriodForSaveDraft(String timePeriod) {
		Logging.info("Clicking on \"save draft\" trigger button and select time period as: " + timePeriod);

		String settings_savedraft_arrow_trigger = PropertyHelper.coreSettings
				.getPropertyValue("settings_savedraft_arrow_trigger");
		String settings_common_boundlist_item = PropertyHelper.coreSettings
				.getPropertyValue("settings_common_boundlist_item").replace("+variable+", timePeriod);

		try {
			Tools.setDriverDefaultValues();
			Tools.scrollElementIntoViewByXpath(settings_savedraft_arrow_trigger);
			Base.base.getDriver().findElement(By.xpath(settings_savedraft_arrow_trigger)).click();
			Base.base.getDriver().findElement(By.xpath(settings_common_boundlist_item)).click();

		} catch (WebDriverException ex) {
			Logging.error("Could not select time period");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 6, 2015<br>
	 * <b>Description</b><br>
	 * select time period for check interval <b>Parameters</b><br>
	 * String timePeriod - time period One of the following item should be provided:
	 * - manually - every minute - every 2 minutes - every 5 minutes - ...
	 */
	public void selectTimePeriodForCheckInterval(String timePeriod) {
		Logging.info("Clicking on \"Check for new messages\" trigger button and select time period as: " + timePeriod);

		String settings_checkinterval_arrow_trigger = PropertyHelper.coreSettings
				.getPropertyValue("settings_checkinterval_arrow_trigger");
		String settings_common_boundlist_item = PropertyHelper.coreSettings
				.getPropertyValue("settings_common_boundlist_item").replace("+variable+", timePeriod);

		try {
			Tools.setDriverDefaultValues();
			Tools.scrollElementIntoViewByXpath(settings_checkinterval_arrow_trigger);
			Base.base.getDriver().findElement(By.xpath(settings_checkinterval_arrow_trigger)).click();
			Base.base.getDriver().findElement(By.xpath(settings_common_boundlist_item)).click();

		} catch (WebDriverException ex) {
			Logging.error("Could not select time period");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 6, 2015<br>
	 * <b>Description</b><br>
	 * select reply prefix for "Plain text message includes original message in
	 * reply using" field <b>Parameters</b><br>
	 * String prefix - reply prefix One of the following item should be provided: -
	 * indentation only - indentation with ">" - indentation with "-"
	 */
	public void selectReplyPrefix(String prefix) {
		Logging.info(
				"Clicking on \"Plain text message includes original message in reply using\" trigger button and select reply prefix as: "
						+ prefix);

		String settings_replyprefix_arrow_trigger = PropertyHelper.coreSettings
				.getPropertyValue("settings_replyprefix_arrow_trigger");
		String settings_common_boundlist_item = PropertyHelper.coreSettings
				.getPropertyValue("settings_common_boundlist_item").replace("+variable+", prefix);

		try {
			Tools.setDriverDefaultValues();
			Tools.scrollElementIntoViewByXpath(settings_replyprefix_arrow_trigger);
			Base.base.getDriver().findElement(By.xpath(settings_replyprefix_arrow_trigger)).click();
			Base.base.getDriver().findElement(By.xpath(settings_common_boundlist_item)).click();

		} catch (WebDriverException ex) {
			Logging.error("Could not select reply prefix");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 7, 2015<br>
	 * <b>Description</b><br>
	 * click on a specified Signature buttons (Add/Edit/Delete)
	 * <b>Parameters</b><br>
	 * String button - Signature buttons One of the following item should be
	 * provided: - Add - Edit - Delete
	 */
	public void clickSignatureButton(String button) {
		Logging.info("Clicking on signature button: " + button);
		String settings_signature_toolbar_button = PropertyHelper.coreSettings
				.getPropertyValue("settings_signature_toolbar_button").replace("+variable+", button);
		boolean disabled = false;

		try {
			Tools.setDriverDefaultValues();
			Tools.scrollElementIntoViewByXpath(settings_signature_toolbar_button);

			disabled = Base.base.getDriver().findElement(By.xpath(settings_signature_toolbar_button))
					.getAttribute("class").contains("x-disabled");
			if (!disabled) {
				Base.base.getDriver().findElement(By.xpath(settings_signature_toolbar_button)).click();
				this.waitForLoadMaskDismissed();
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not click on Signature Button " + button);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 7, 2015<br>
	 * <b>Description</b><br>
	 * click on a specified tool buttons in Signature edit window
	 * <b>Parameters</b><br>
	 * String button - tool buttons in Signature edit window One of the following
	 * item should be provided: - Save - Cancel
	 */
	public void clickToolButtonInSignatureEditWindow(String button) {
		Logging.info("Clicking on tool button in signature edit window: " + button);
		String settings_signature_window_tool_button = PropertyHelper.coreSettings
				.getPropertyValue("settings_signature_window_tool_button").replace("+variable+", button);

		try {
			Tools.setDriverDefaultValues();
			Tools.scrollElementIntoViewByXpath(settings_signature_window_tool_button);

			Base.base.getDriver().findElement(By.xpath(settings_signature_window_tool_button)).click();
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on tool button in Signature edit window " + button);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 07, 2015<br>
	 * <b>Description</b><br>
	 * type text in name field in Signature edit window <b>Parameters</b><br>
	 * String text: the input content
	 */
	public void typeInNameFieldInSignatureWindow(String text) {
		Logging.info("type text in name field in Signature edit window");

		String settings_signature_name = PropertyHelper.coreSettings.getPropertyValue("settings_signature_name");

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(settings_signature_name)).clear();
			Base.base.getDriver().findElement(By.xpath(settings_signature_name)).sendKeys(text);
		} catch (WebDriverException ex) {
			Logging.error("Could not type text in name field in Signature edit window");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 07, 2015<br>
	 * <b>Description</b><br>
	 * type text in body field in Signature edit window <b>Parameters</b><br>
	 * String text: the input content
	 */
	public void typeInBodyFieldInSignatureWindow(String text) {
		Logging.info("type text in body field in Signature edit window");

		String settings_signature_body = PropertyHelper.coreSettings.getPropertyValue("settings_signature_body");

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(settings_signature_body)).clear();
			Base.base.getDriver().findElement(By.xpath(settings_signature_body)).sendKeys(text);
		} catch (WebDriverException ex) {
			Logging.error("Could not type text in body field in Signature edit window");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 07, 2015<br>
	 * <b>Description</b><br>
	 * check the Signature as default in Signature edit window <b>Parameters</b><br>
	 * boolean check: True or False for check and uncheck
	 */
	public void checkNewSignatureAsDefaultSignature(boolean check) {
		Logging.info("check this signature as default: " + check);

		String settings_signature_default_checkbox = PropertyHelper.coreSettings
				.getPropertyValue("settings_signature_default_checkbox");
		String settings_signature_default_checkbox_flag = PropertyHelper.coreSettings
				.getPropertyValue("settings_signature_default_checkbox_flag");

		try {
			Tools.setDriverDefaultValues();

			boolean isChecked = Base.base.getDriver().findElement(By.xpath(settings_signature_default_checkbox_flag))
					.getAttribute("class").contains("cb-checked");

			if (check) {
				if (!isChecked) {
					Base.base.getDriver().findElement(By.xpath(settings_signature_default_checkbox)).click();
				}
				Logging.info("Check this signature as default");
			} else {
				if (isChecked) {
					Base.base.getDriver().findElement(By.xpath(settings_signature_default_checkbox)).click();
				}
				Logging.info("Uncheck this signature as default");
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not check or uncheck this signature as default");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 07, 2015<br>
	 * <b>Description</b><br>
	 * verify signature exists in signature list by name <b>Parameters</b><br>
	 * String name: signature name
	 */
	public boolean verifySignatureExistenceByName(String name) {
		Logging.info("Verifying signature existence in list by name: " + name);
		name = name.trim();
		String settings_signature_list_by_name = PropertyHelper.coreSettings
				.getPropertyValue("settings_signature_list_by_name").replace("+variable+", name);
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(settings_signature_list_by_name));
			// signature is found.
			result = true;
			Logging.info("Found signature by name: " + name);
		} catch (WebDriverException ex) {
			Logging.info("Could not found signature by name: " + name);
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 07, 2015<br>
	 * <b>Description</b><br>
	 * click on signature in signature list by name <b>Parameters</b><br>
	 * String name: signature name
	 */
	public void clickOnSignatureInListByName(String name) {
		Logging.info("Clicking on signature in list by name: " + name);
		name = name.trim();
		String settings_signature_list_by_name = PropertyHelper.coreSettings
				.getPropertyValue("settings_signature_list_by_name").replace("+variable+", name);

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(settings_signature_list_by_name)).click();

		} catch (WebDriverException ex) {
			Logging.info("Could not click on signature by name: " + name);
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 08, 2015<br>
	 * <b>Description</b><br>
	 * cleanup all signatures in list
	 * 
	 */
	public void cleanupAllSignatures() {
		Logging.info("Cleanup all signatures in list");
		String settings_signature_list = PropertyHelper.coreSettings.getPropertyValue("settings_signature_list");

		try {
			Tools.setDriverDefaultValues();
			List<WebElement> signatures = Base.base.getDriver().findElements(By.xpath(settings_signature_list));
			if (signatures.size() != 0) {
				for (WebElement signature : signatures) {
					signature.click();
					this.clickSignatureButton("Delete");
					this.clickButtonOnMessageBox("Yes");
				}
			} else {
				Logging.info("there is no any signatures in list");
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not cleanup the signatures");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 08, 2015<br>
	 * <b>Description</b><br>
	 * verify signature is default in signature list by name <b>Parameters</b><br>
	 * String name: signature name
	 */
	public boolean verifySignatureDefaultFlagByName(String name) {
		Logging.info("Verifying signature is default in list by name: " + name);
		name = name.trim();
		String settings_signature_default_flag_by_name = PropertyHelper.coreSettings
				.getPropertyValue("settings_signature_default_flag_by_name").replace("+variable+", name);
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(settings_signature_default_flag_by_name));
			// default flag is found.
			result = true;
			Logging.info("Found default flag by name: " + name);
		} catch (WebDriverException ex) {
			Logging.info("Could not verify signature is default in list by name: " + name);
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 08, 2015<br>
	 * <b>Description</b><br>
	 * check "Include signature" button <b>Parameters</b><br>
	 * boolean check: True or False for check and uncheck
	 */
	public void checkIncludeSignatureButton(boolean check) {
		Logging.info("check \"Include signature\" button: " + check);

		String settings_signature_auto_insert_checkbox = PropertyHelper.coreSettings
				.getPropertyValue("settings_signature_auto_insert_checkbox");
		String settings_signature_auto_insert_checkbox_flag = PropertyHelper.coreSettings
				.getPropertyValue("settings_signature_auto_insert_checkbox_flag");

		try {
			Tools.setDriverDefaultValues();

			boolean isChecked = Base.base.getDriver()
					.findElement(By.xpath(settings_signature_auto_insert_checkbox_flag)).getAttribute("class")
					.contains("cb-checked");

			if (check) {
				if (!isChecked) {
					Base.base.getDriver().findElement(By.xpath(settings_signature_auto_insert_checkbox)).click();
				}
				Logging.info("Check \"Include signature\" button");
			} else {
				if (isChecked) {
					Base.base.getDriver().findElement(By.xpath(settings_signature_auto_insert_checkbox)).click();
				}
				Logging.info("Uncheck \"Include signature\" button");
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not check or uncheck \"Include signature\" button");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 09, 2015<br>
	 * <b>Description</b><br>
	 * select "Put my signature" <b>Parameters</b><br>
	 * String button: signature position button One of the following item should be
	 * provided: - Below the original email - Above the original email
	 */
	public void selectSignaturePosition(String button) {
		Logging.info("select signature position as: " + button);

		String settings_common_boundlist_item = PropertyHelper.coreSettings
				.getPropertyValue("settings_common_boundlist_item").replace("+variable+", button);
		String settings_signature_position_trigger_button = PropertyHelper.coreSettings
				.getPropertyValue("settings_signature_position_trigger_button");

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(settings_signature_position_trigger_button)).click();
			Base.base.getDriver().findElement(By.xpath(settings_common_boundlist_item)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not select signature position as: " + button);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 10, 2015<br>
	 * <b>Description</b><br>
	 * type text in rule name field and choose whether to activate the rule
	 * accordingly <b>Parameters</b><br>
	 * String name: rule name boolean active: true - to active / false - not
	 * inactive
	 */
	public void typeRuleNameAndEnableActiveInMessageFiltersWindow(String name, boolean active) {
		Logging.info("Typing in rule name: " + name + " , and set this rule to be " + (active ? "active" : "inactive"));

		String settings_message_filters_rule_name = PropertyHelper.coreSettings
				.getPropertyValue("settings_message_filters_rule_name");
		String settings_message_filters_active_table = PropertyHelper.coreSettings
				.getPropertyValue("settings_message_filters_active_table");
		String settings_message_filters_active_button = PropertyHelper.coreSettings
				.getPropertyValue("settings_message_filters_active_button");

		try {
			Tools.setDriverDefaultValues();

			// Enter rule name.
			Base.base.getDriver().findElement(By.xpath(settings_message_filters_rule_name)).clear();
			Base.base.getDriver().findElement(By.xpath(settings_message_filters_rule_name)).sendKeys(name);

			// Set active or inactive.
			boolean isActive = Base.base.getDriver().findElement(By.xpath(settings_message_filters_active_table))
					.getAttribute("class").contains("cb-checked");

			if ((active && !isActive) || (!active && isActive)) {
				Base.base.getDriver().findElement(By.xpath(settings_message_filters_active_button)).click();
			}

		} catch (WebDriverException ex) {
			Logging.error("Could not type rule name and enable active in message filter window");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 10, 2015<br>
	 * <b>Description</b><br>
	 * select "contains" or "does not contain" for a specific field and then typing
	 * expected text in Message Filters step 1. <b>Parameters</b><br>
	 * String field: the field specified One of the following item should be
	 * provided: - Subject - From - To - CC - To or CC boolean contain: true -
	 * contains / false - does not contain String text - text to be typed in,
	 * subject or email address
	 */
	public void inputConditionForFieldInMessageFiltersWindow(String field, boolean contain, String text) {
		Logging.info("Setting rule applied emails: " + field + ", " + (contain ? "contains" : "does not contain")
				+ " text " + text);

		field = field.trim();
		String settings_message_filters_email_dropdown_list = PropertyHelper.coreSettings
				.getPropertyValue("settings_message_filters_email_dropdown_list").replace("+variable+", field);
		String settings_message_filters_email_dropdown_item = PropertyHelper.coreSettings
				.getPropertyValue("settings_message_filters_email_dropdown_item");
		String settings_message_filters_email_text_input = PropertyHelper.coreSettings
				.getPropertyValue("settings_message_filters_email_text_input").replace("+variable+", field);

		try {
			Tools.setDriverDefaultValues();

			// Select item from dropdown list.
			Base.base.getDriver().findElement(By.xpath(settings_message_filters_email_dropdown_list)).click();
			if (contain) {
				settings_message_filters_email_dropdown_item = settings_message_filters_email_dropdown_item
						.replace("+variable+", "contains");
			} else {
				settings_message_filters_email_dropdown_item = settings_message_filters_email_dropdown_item
						.replace("+variable+", "does not contain");
			}
			Base.base.getDriver().findElement(By.xpath(settings_message_filters_email_dropdown_item)).click();

			// Type in text.
			Base.base.getDriver().findElement(By.xpath(settings_message_filters_email_text_input)).clear();
			Base.base.getDriver().findElement(By.xpath(settings_message_filters_email_text_input)).sendKeys(text);

		} catch (WebDriverException ex) {
			Logging.error("Could not perform actions in message filters window - set applied emails");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 10, 2015<br>
	 * <b>Description</b><br>
	 * select action for Message Filters step 2 <b>Parameters</b><br>
	 * String action: the action field One of the following item should be provided:
	 * - MoveToFolder - ForwardToEmail String text: email address to be typed in or
	 * folder name to be selected
	 */
	public void setAppliedActionInMessageFiltersWindow(String action, String text) {
		Logging.info("Setting rule applied action: " + action);

		action = action.trim();
		text = text.trim();
		String settings_message_filters_action_action_trigger = PropertyHelper.coreSettings
				.getPropertyValue("settings_message_filters_action_action_trigger");
		String settings_message_filters_action_email_input = PropertyHelper.coreSettings
				.getPropertyValue("settings_message_filters_action_email_input");
		String settings_message_filters_action_dropdown_item = PropertyHelper.coreSettings
				.getPropertyValue("settings_message_filters_action_dropdown_item");
		String settings_message_filters_action_folder_trigger = PropertyHelper.coreSettings
				.getPropertyValue("settings_message_filters_action_folder_trigger");

		try {
			Tools.setDriverDefaultValues();

			// Select item from dropdown list 1.
			Base.base.getDriver().findElement(By.xpath(settings_message_filters_action_action_trigger)).click();
			String actionItem = settings_message_filters_action_dropdown_item.replace("+variable+", action);
			Base.base.getDriver().findElement(By.xpath(actionItem)).click();

			// Select folder or input email address
			if (action.equals("MoveToFolder")) {
				// Select a folder from dropdown list.
				Base.base.getDriver().findElement(By.xpath(settings_message_filters_action_folder_trigger)).click();
				String selectFolder = settings_message_filters_action_dropdown_item.replace("+variable+", text);
				Base.base.getDriver().findElement(By.xpath(selectFolder)).click();
			} else if (action.equals("ForwardToEmail")) {
				// Type in the input box.
				Base.base.getDriver().findElement(By.xpath(settings_message_filters_action_email_input)).sendKeys(text);
			}

		} catch (WebDriverException ex) {
			Logging.error("Could not perform actions in message filters window - set applied emails");
			throw ex;
		}
		Tools.waitFor(1, TimeUnit.SECONDS);
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 11, 2015<br>
	 * <b>Description</b><br>
	 * select action for Message Filters step 2 <b>Parameters</b><br>
	 * String action: the action field One of the following item should be provided:
	 * - Discard
	 */
	public void setAppliedActionInMessageFiltersWindow(String action) {
		Logging.info("Setting rule applied action: " + action);

		action = action.trim();
		String settings_message_filters_action_action_trigger = PropertyHelper.coreSettings
				.getPropertyValue("settings_message_filters_action_action_trigger");
		String settings_message_filters_action_dropdown_item = PropertyHelper.coreSettings
				.getPropertyValue("settings_message_filters_action_dropdown_item");

		try {
			Tools.setDriverDefaultValues();

			// Select item from dropdown list 1.
			Base.base.getDriver().findElement(By.xpath(settings_message_filters_action_action_trigger)).click();
			String actionItem = settings_message_filters_action_dropdown_item.replace("+variable+", action);
			Base.base.getDriver().findElement(By.xpath(actionItem)).click();

		} catch (WebDriverException ex) {
			Logging.error("Could not set rule applied action: " + action);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 10, 2015<br>
	 * <b>Description</b><br>
	 * click on Save/Cancel button in message filters window. <b>Parameters</b><br>
	 * String button: Save/Cancel should be supplied
	 */
	public void clickButtonInMessageFiltersWindow(String button) {
		Logging.info("Clicking button on message filters window: " + button);

		String settings_message_filters_button = PropertyHelper.coreSettings
				.getPropertyValue("settings_message_filters_button").replace("+variable+", button);
		Logging.info(button + "=" + settings_message_filters_button);

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(settings_message_filters_button)).click();
			this.waitForLoadMaskDismissed();

		} catch (WebDriverException ex) {
			Logging.error("Could not click button on message filters window");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 11, 2015<br>
	 * <b>Description</b><br>
	 * click on rule in rule list by name <b>Parameters</b><br>
	 * String name: rule name
	 */
	public void clickOnRuleInListByName(String name) {
		Logging.info("Clicking on rule in list by name: " + name);
		name = name.trim();
		String settings_message_filters_rule_item_by_name = PropertyHelper.coreSettings
				.getPropertyValue("settings_message_filters_rule_item_by_name").replace("+variable+", name);

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(settings_message_filters_rule_item_by_name)).click();

		} catch (WebDriverException ex) {
			Logging.info("Could not click on rule by name: " + name);
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 11, 2015<br>
	 * <b>Description</b><br>
	 * delete rule in rule list by name <b>Parameters</b><br>
	 * String name: rule name
	 */
	public void deleteRuleInListByName(String name) {
		Logging.info("Deleting rule in list by name: " + name);
		name = name.trim();

		try {
			Tools.setDriverDefaultValues();
			this.clickOnRuleInListByName(name);
			this.clickSettingsItem("Delete");

		} catch (WebDriverException ex) {
			Logging.info("Could not delete rule by name: " + name);
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 11, 2015<br>
	 * <b>Description</b><br>
	 * cleanup all rules in list
	 * 
	 */
	public void cleanupAllRules() {
		Logging.info("Cleanup all rules in list");
		String settings_message_filters_rule_item_in_list = PropertyHelper.coreSettings
				.getPropertyValue("settings_message_filters_rule_item_in_list");

		try {
			Tools.setDriverDefaultValues();
			List<WebElement> rules = Base.base.getDriver()
					.findElements(By.xpath(settings_message_filters_rule_item_in_list));
			if (rules.size() != 0) {
				for (WebElement rule : rules) {
					rule.click();
					this.clickToolbarButtonInSettingsDetail("Delete");
					this.clickButtonOnMessageBoxIfDisplayed("Yes");
				}
			} else {
				Logging.info("there is no any rules in list");
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not cleanup the rules");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 11, 2015<br>
	 * <b>Description</b><br>
	 * verify rule exists in rule list by name <b>Parameters</b><br>
	 * String name: rule name
	 */
	public boolean verifyRuleExistenceByName(String name) {
		Logging.info("Verifying rule existence in list by name: " + name);
		name = name.trim();
		String settings_message_filters_rule_item_by_name = PropertyHelper.coreSettings
				.getPropertyValue("settings_message_filters_rule_item_by_name").replace("+variable+", name);
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(settings_message_filters_rule_item_by_name));
			// signature is found.
			result = true;
			Logging.info("Found rule by name: " + name);
		} catch (WebDriverException ex) {
			Logging.info("Could not found rule by name: " + name);
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 14, 2015<br>
	 * <b>Description</b><br>
	 * check "Auto forward" button <b>Parameters</b><br>
	 * boolean check: true - to enable / false - to disable
	 */
	public void checkAutoForward(boolean check) {
		Logging.info("check \"Auto forward\" as: " + (check ? "enable" : "disable"));

		String settings_auto_forward_enable_table = PropertyHelper.coreSettings
				.getPropertyValue("settings_auto_forward_enable_table");
		String settings_auto_forward_enable_button = PropertyHelper.coreSettings
				.getPropertyValue("settings_auto_forward_enable_button");

		try {
			Tools.setDriverDefaultValues();

			// Set enable or disable.
			boolean isEnabled = Base.base.getDriver().findElement(By.xpath(settings_auto_forward_enable_table))
					.getAttribute("class").contains("cb-checked");

			if ((check && !isEnabled) || (!check && isEnabled)) {
				Base.base.getDriver().findElement(By.xpath(settings_auto_forward_enable_button)).click();
			}

		} catch (WebDriverException ex) {
			Logging.error("Could not check \"Auto forward\" as: " + (check ? "enable" : "disable"));
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 14, 2015<br>
	 * <b>Description</b><br>
	 * clear all email addresses in destination field in Auto-forward window
	 */
	public void clearAllEmailInDestinationInAutoForward() {
		Logging.info("clear all email addresses in destination field in Auto-forward window");

		String settings_auto_forward_destination_remove_bubble = PropertyHelper.coreSettings
				.getPropertyValue("settings_auto_forward_destination_remove_bubble");

		try {
			Tools.setDriverDefaultValues();

			List<WebElement> recipients = Base.base.getDriver()
					.findElements(By.xpath(settings_auto_forward_destination_remove_bubble));
			if (recipients.size() != 0) {
				for (WebElement removeBubble : recipients) {
					removeBubble.click();
				}
			} else {
				Logging.info("there is no any recipient in destinatin field");
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not clear all email addresses in destination field");
			throw ex;
		}
		Tools.waitFor(2, TimeUnit.SECONDS);
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 14, 2015<br>
	 * <b>Description</b><br>
	 * type in email address in destination field in Auto-forward window
	 * <b>Parameters</b><br>
	 * String email: email address of destination
	 */
	public void typeEmailInDestinationInAutoForward(String email) {
		Logging.info("type email in destination field in Auto-forward window: " + email);

		String settings_auto_forward_destination_input = PropertyHelper.coreSettings
				.getPropertyValue("settings_auto_forward_destination_input");

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(settings_auto_forward_destination_input)).sendKeys(email);
			Tools.waitFor(2, TimeUnit.SECONDS);
			Base.base.getDriver().findElement(By.xpath(settings_auto_forward_destination_input)).sendKeys(Keys.ENTER);

			// send a semicolon to make sure this address is accepted
			Base.base.getDriver().findElement(By.xpath(settings_auto_forward_destination_input))
					.sendKeys(Keys.SEMICOLON);

		} catch (WebDriverException ex) {
			Logging.error("Could not type email as destination");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Nov 21, 2016<br>
	 * <b>Description</b><br>
	 * type in text in destination field in Auto-forward window
	 * <b>Parameters</b><br>
	 * String text: the string that is to be type in destination field
	 */
	public void typeTextInDestinationInAutoForward(String text) {
		Logging.info("type text in destination field in Auto-forward window: " + text);

		String settings_auto_forward_destination_input = PropertyHelper.coreSettings
				.getPropertyValue("settings_auto_forward_destination_input");

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(settings_auto_forward_destination_input)).sendKeys(text);

		} catch (WebDriverException ex) {
			Logging.error("Could not type text in destination field");
			throw ex;
		}

		Tools.waitFor(1, TimeUnit.SECONDS);
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 14, 2015<br>
	 * <b>Description</b><br>
	 * check "Keep a copy" button in auto-forward window <b>Parameters</b><br>
	 * boolean check: true - to enable / false - to disable
	 */
	public void checkKeepCopyInAutoForwardWindow(boolean check) {
		Logging.info("check \"Keep a copy\" in auto-forward window as: " + (check ? "enable" : "disable"));

		String settings_auto_forward_keepcopy_table = PropertyHelper.coreSettings
				.getPropertyValue("settings_auto_forward_keepcopy_table");
		String settings_auto_forward_keepcopy_button = PropertyHelper.coreSettings
				.getPropertyValue("settings_auto_forward_keepcopy_button");

		try {
			Tools.setDriverDefaultValues();
			// Set enable or disable.
			boolean isEnabled = Base.base.getDriver().findElement(By.xpath(settings_auto_forward_keepcopy_table))
					.getAttribute("class").contains("cb-checked");
			if ((check && !isEnabled) || (!check && isEnabled)) {
				Base.base.getDriver().findElement(By.xpath(settings_auto_forward_keepcopy_button)).click();
			}

		} catch (WebDriverException ex) {
			Logging.error("Could not check \"Keep a copy\" as: " + (check ? "enable" : "disable"));
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 14, 2015<br>
	 * <b>Description</b><br>
	 * check "Auto-reply" button in auto-reply window <b>Parameters</b><br>
	 * boolean check: true - to enable / false - to disable
	 */
	public void checkAutoReply(boolean check) {
		Logging.info("check \"Turn on auto-replies\" in auto-reply window as: " + (check ? "enable" : "disable"));

		String settings_auto_reply_enable_tbody = PropertyHelper.coreSettings
				.getPropertyValue("settings_auto_reply_enable_tbody");
		String settings_auto_reply_enable_button = PropertyHelper.coreSettings
				.getPropertyValue("settings_auto_reply_enable_button");

		try {
			Tools.setDriverDefaultValues();

			// Set enable or disable.
			boolean isEnabled = Base.base.getDriver().findElement(By.xpath(settings_auto_reply_enable_tbody))
					.getAttribute("class").contains("cb-checked");

			if ((check && !isEnabled) || (!check && isEnabled)) {
				Base.base.getDriver().findElement(By.xpath(settings_auto_reply_enable_button)).click();
			}

		} catch (WebDriverException ex) {
			Logging.error("Could not check \"Turn on auto-replies\" as: " + (check ? "enable" : "disable"));
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 07, 2015<br>
	 * <b>Description</b><br>
	 * type replied message body in message field in Auto-reply window
	 * <b>Parameters</b><br>
	 * String text: the input content
	 */
	public void typeInMessageBodyFieldInAutoReplyWindow(String text) {
		Logging.info("typing text in replied message body field in Auto-reply window");

		String settings_auto_reply_message_input = PropertyHelper.coreSettings
				.getPropertyValue("settings_auto_reply_message_input");

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(settings_auto_reply_message_input)).clear();
			Base.base.getDriver().findElement(By.xpath(settings_auto_reply_message_input)).sendKeys(text);
		} catch (WebDriverException ex) {
			Logging.error("Could not type text in replied message body field in Auto-reply window");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Charlie Li<br>
	 * <b>Date</b>: August 30, 2017<br>
	 * <b>Description</b><br>
	 * Verify message body in message field in Auto-reply window
	 * <b>Parameters</b><br>
	 * String text: message body
	 */
	public boolean verifyMessageBodyFieldInAutoReplyWindow(String text) {

		Logging.info("Verify message body in message field in Auto-reply is " + text);
		boolean result = false;
		String settings_auto_reply_message_input = PropertyHelper.coreSettings
				.getPropertyValue("settings_auto_reply_message_input");
		try {
			Tools.setDriverDefaultValues();
			if (text.equals(Base.base.getDriver().findElement(By.xpath(settings_auto_reply_message_input))
					.getAttribute("value"))) {
				result = true;
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not find replied message body field in Auto-reply window");
			throw ex;
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 14, 2015<br>
	 * <b>Description</b><br>
	 * check "Return original email with the auto-reply message" button in
	 * auto-reply window <b>Parameters</b><br>
	 * boolean check: true - to enable / false - to disable
	 */
	public void checkReturnOriginalMessageInAutoReplyWindow(boolean check) {
		Logging.info("check \"Return original email with the auto-reply message\" in auto-reply window as: "
				+ (check ? "enable" : "disable"));

		String settings_auto_reply_return_orig_tbody = PropertyHelper.coreSettings
				.getPropertyValue("settings_auto_reply_return_orig_tbody");
		String settings_auto_reply_return_orig_button = PropertyHelper.coreSettings
				.getPropertyValue("settings_auto_reply_return_orig_button");

		try {
			Tools.setDriverDefaultValues();

			// Set enable or disable.
			boolean isEnabled = Base.base.getDriver().findElement(By.xpath(settings_auto_reply_return_orig_tbody))
					.getAttribute("class").contains("cb-checked");

			if ((check && !isEnabled) || (!check && isEnabled)) {
				Base.base.getDriver().findElement(By.xpath(settings_auto_reply_return_orig_button)).click();
			}

		} catch (WebDriverException ex) {
			Logging.error("Could not check \"Return original email with the auto-reply message\" as: "
					+ (check ? "enable" : "disable"));
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 09, 2015<br>
	 * <b>Description</b><br>
	 * select "Auto-Suggest" button <b>Parameters</b><br>
	 * boolean enable: Auto-Suggest enable button
	 */
	public void selectAutoSuggestButton(boolean enable) {
		Logging.info("selecting \"Auto-Suggest\" button as: " + enable);

		String settings_auto_suggest_enable_button = PropertyHelper.coreSettings
				.getPropertyValue("settings_auto_suggest_enable_button");
		String settings_auto_suggest_enable_table = PropertyHelper.coreSettings
				.getPropertyValue("settings_auto_suggest_enable_table");

		try {
			Tools.setDriverDefaultValues();

			boolean isChecked = Base.base.getDriver().findElement(By.xpath(settings_auto_suggest_enable_table))
					.getAttribute("class").contains("cb-checked");

			if (isChecked ^ enable) {
				Base.base.getDriver().findElement(By.xpath(settings_auto_suggest_enable_button)).click();
				Logging.info("\"Auto-Suggest\" button is set as: " + enable);
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not select \"Auto-Suggest\" button as: " + enable);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 17, 2015<br>
	 * <b>Description</b><br>
	 * select "Block images" radio button <b>Parameters</b><br>
	 * String button: Block image radio button One of the following item should be
	 * provided: - Allow all images - Allow from my contacts only - Allow all except
	 * spam - Block all images
	 */
	public void selectBlockImageRadioButton(String button) {
		Logging.info("select \"Block images\" radio button as: " + button);

		String settings_block_image_radio_button = PropertyHelper.coreSettings
				.getPropertyValue("settings_block_image_radio_button").replace("+variable+", button);
		String settings_block_image_radio_table = PropertyHelper.coreSettings
				.getPropertyValue("settings_block_image_radio_table").replace("+variable+", button);

		try {
			Tools.setDriverDefaultValues();

			boolean isChecked = Base.base.getDriver().findElement(By.xpath(settings_block_image_radio_table))
					.getAttribute("class").contains("cb-checked");

			if (!isChecked) {
				Base.base.getDriver().findElement(By.xpath(settings_block_image_radio_button)).click();
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not select \"Block images\" radio button as: " + button);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 19, 2015<br>
	 * <b>Description</b><br>
	 * select items for specific fields in Calendar <b>Parameters</b><br>
	 * String option: the selected field One of the following item should be
	 * provided: - Default view - Start day at - End day at - Start week on -
	 * Default event duration String item: selected item
	 */
	public void selectCalendarSettings(String field, String item) {
		Logging.info("Selecting item: " + item + " in calendar settings field: " + field);

		field = field.trim();
		item = item.trim();
		String settings_calendar_item_trigger = PropertyHelper.coreSettings
				.getPropertyValue("settings_calendar_item_trigger").replace("+variable+", field);
		String settings_calendar_boundlist_item = PropertyHelper.coreSettings
				.getPropertyValue("settings_calendar_boundlist_item").replace("+variable+", item);
		String settings_calendar_boundlist_item_for_daystart = PropertyHelper.coreSettings
				.getPropertyValue("settings_calendar_boundlist_item_for_daystart").replace("+variable+", item);
		String settings_calendar_boundlist_item_for_dayend = PropertyHelper.coreSettings
				.getPropertyValue("settings_calendar_boundlist_item_for_dayend").replace("+variable+", item);
		String boundlistItem;

		List<String> itemsForDefaultView = Arrays.asList("Day", "Week", "Month", "List");
		List<String> itemsForDayStartEnd = Arrays.asList("12:00 AM", "1:00 AM", "2:00 AM", "3:00 AM", "4:00 AM",
				"5:00 AM", "6:00 AM", "7:00 AM", "8:00 AM", "9:00 AM", "10:00 AM", "11:00 AM", "12:00 PM", "1:00 PM",
				"2:00 PM", "3:00 PM", "4:00 PM", "5:00 PM", "6:00 PM", "7:00 PM", "8:00 PM", "9:00 PM", "10:00 PM",
				"11:00 PM");
		List<String> itemsForWeekStart = Arrays.asList("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
				"Saturday");
		List<String> itemsForEventDuration = Arrays.asList("Minutes (1 - 60)", "Hours (1 - 24)");
		List<String> itemList = new ArrayList<String>();

		try {
			Tools.setDriverDefaultValues();
			if (field.equals("Default view")) {
				itemList.addAll(itemsForDefaultView);
			} else if (field.equals("Start day at") || field.equals("End day at")) {
				itemList.addAll(itemsForDayStartEnd);
			} else if (field.equals("Start week on")) {
				itemList.addAll(itemsForWeekStart);
			} else if (field.equals("Default event duration")) {
				itemList.addAll(itemsForEventDuration);
			}

			if (!itemList.contains(item)) {
				Logging.error("\"" + item + "\" is not a item for field: " + field);
				throw new WebDriverException();
			}

			// click on item trigger
			Base.base.getDriver().findElement(By.xpath(settings_calendar_item_trigger)).click();

			// Select item from dropdown list
			if (field.equals("Start day at")) {
				boundlistItem = settings_calendar_boundlist_item_for_daystart;
			} else if (field.equals("End day at")) {
				boundlistItem = settings_calendar_boundlist_item_for_dayend;
			} else {
				boundlistItem = settings_calendar_boundlist_item;
			}

			Tools.scrollElementIntoViewByXpath(boundlistItem);
			Base.base.getDriver().findElement(By.xpath(boundlistItem)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not select item: " + item + " in calendar settings field: " + field);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 19, 2015<br>
	 * <b>Description</b><br>
	 * type positive integer in event duration field in Calendar settings
	 * <b>Parameters</b><br>
	 * String number: the input number, only positive integer is acceptable
	 */
	public void typeNumberInEventDurationFieldInCalendar(String value) {
		Logging.info("Inputing  duration time:" + value + " in event duration field in Calendar settings. ");

		String settings_calendar_eventduration_input = PropertyHelper.coreSettings
				.getPropertyValue("settings_calendar_eventduration_input");
		String settings_calendar_eventduration_option = PropertyHelper.coreSettings
				.getPropertyValue("settings_calendar_eventduration_option").replace("+variable+", value);

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(settings_calendar_eventduration_input)).click();
			Base.base.getDriver().findElement(By.xpath(settings_calendar_eventduration_option)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not select option " + value + " in event duration field in Calendar settings");
			throw ex;
		}
	}

	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jul 25, 2016<br>
	 * <b>Description: </b><br>
	 * Clicks on trigger button of auto-reply date picker.
	 * 
	 * @param pickerTrigger
	 *            - only constant names AUTO_REPLY_START_DATE or AUTO_REPLY_END_DATE
	 *            should be used.
	 */
	public void clickOnAutoReplyDateTrigger(String pickerTrigger) {
		Logging.info("Clicking on " + pickerTrigger);
		String triggerBtn = null;
		if (pickerTrigger.equals(AUTO_REPLY_START_DATE)) {
			triggerBtn = PropertyHelper.coreSettings.getPropertyValue("settings_auto_reply_start_date_trigger");
		} else if (pickerTrigger.equals(AUTO_REPLY_END_DATE)) {
			triggerBtn = PropertyHelper.coreSettings.getPropertyValue("settings_auto_reply_end_date_trigger");
		} else {
			throw new IllegalArgumentException("Invalid option, only constant names "
					+ "AUTO_REPLY_START_DATE or AUTO_REPLY_END_DATE should be used.");
		}

		try {
			Tools.setDriverDefaultValues();
			Tools.waitFor(2, TimeUnit.SECONDS);
			Base.base.getDriver().findElement(By.xpath(triggerBtn)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on " + pickerTrigger);
			throw ex;
		}
	}

	/**
	 * <b>Author: </b>Vivian Xie<br>
	 * <b>Date: </b>Nov 14, 2016<br>
	 * <b>Description: </b><br>
	 * Clicks on Auto-Suggest Item.
	 * 
	 * @param itemName
	 *            - the item text
	 */
	public void clickAutoSuggustItem(String itemName) {
		Logging.info("Clicking SuggestItem " + itemName);
		String settings_suggestlist_recipient = PropertyHelper.coreSettings
				.getPropertyValue("settings_suggestlist_recipient").replace("+variable+", itemName);

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(settings_suggestlist_recipient)).click();
			Tools.waitFor(1, TimeUnit.SECONDS);
		} catch (WebDriverException ex) {
			Logging.error("Could not click on " + settings_suggestlist_recipient);
			throw ex;
		}
	}

	/**
	 * <b>Author: </b>Vivian Xie<br>
	 * <b>Date: </b>Nov 14, 2016<br>
	 * <b>Description: </b><br>
	 * verify the specific recipient has been added.
	 * 
	 * @param recipientName
	 *            - recipient name
	 */
	public boolean verifyRecipientBeenAdded(String recipientName) {
		Logging.info("verify Recipient " + recipientName);
		String settings_recipient_Input_Recipient = PropertyHelper.coreSettings
				.getPropertyValue("settings_recipient_Input_Recipient").replace("+variable+", recipientName);
		boolean result = false;
		try {
			Tools.setDriverDefaultValues();
			Tools.waitUntilElementDisplayedByXpath(settings_recipient_Input_Recipient, 5);
			result = true;
		} catch (WebDriverException ex) {
			Logging.error("Could not find element in  verifyRecipientBeenAdded");
			throw ex;
		}
		return result;
	}

	/**
	 * <b>Author: </b>Vivian Xie<br>
	 * <b>Date: </b>Nov 14, 2016<br>
	 * <b>Description: </b><br>
	 * delete all external accounts
	 */
	public void deleteAllExternalAccounts() {
		Logging.info("Deleting all external account");
		String settings_mail_external_account = PropertyHelper.coreSettings
				.getPropertyValue("settings_mail_external_account");

		try {
			Tools.setDriverDefaultValues();
			List<WebElement> externalAccounts = Base.base.getDriver()
					.findElements(By.xpath(settings_mail_external_account));
			if (externalAccounts.size() != 0) {
				for (WebElement account : externalAccounts) {
					account.click();
					this.clickToolbarButtonInSettingsDetail("Delete");
					this.clickButtonOnMessageBoxIfDisplayed("Yes");
				}
			} else {
				Logging.info("there is no external account in list");
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not cleanup external acounts");
			throw ex;
		}
	}

	/**
	 * <b>Author: </b>Vivian Xie<br>
	 * <b>Date: </b>Nov 29, 2016<br>
	 * <b>Description: </b><br>
	 * click on protocol Input box on adding external account page
	 */
	public void clickProtocolInputOfExternalAccount() {
		Logging.info("Starting to input external account protocol");
		String settings_external_account_protocal_input = PropertyHelper.coreSettings
				.getPropertyValue("settings_external_account_protocal_input");
		try {
			Base.base.getDriver().findElement(By.xpath(settings_external_account_protocal_input)).click();
		} catch (WebDriverException ex) {
			Logging.info("Could not click on  external account protocol input box");
			throw ex;
		}
	}

	/**
	 * <b>Author: </b>Vivian Xie<br>
	 * <b>Date: </b>Nov 29, 2016<br>
	 * <b>Description: </b><br>
	 * Select the given protocol option <b>parameter: </b>String option<br>
	 * value can be IMAP, POP
	 */
	public void clickProtalOptionOfExternalAccount(String option) {
		Logging.info("Starting to input external account protocol option:" + option);
		String settings_external_account_protocal_option = PropertyHelper.coreSettings
				.getPropertyValue("settings_external_account_protocal_option").replace("+variable+", option);
		Logging.info(settings_external_account_protocal_option);
		try {
			Base.base.getDriver().findElement(By.xpath(settings_external_account_protocal_option)).click();
		} catch (WebDriverException ex) {
			Logging.info("Could not input external account protocol option ");
			throw ex;
		}
	}

	/**
	 * <b>Author: </b>Vivian Xie<br>
	 * <b>Date: </b>Dec 21, 2016<br>
	 * <b>Description: </b><br>
	 * click on Service Provider Input box on adding external account page
	 */
	public void clickServiceProviderInputOfExternalAccount() {
		Logging.info("Starting to input external account service provider");
		String settings_external_account_service_provider_input = PropertyHelper.coreSettings
				.getPropertyValue("settings_external_account_service_provider_input");
		try {
			Base.base.getDriver().findElement(By.xpath(settings_external_account_service_provider_input)).click();
		} catch (WebDriverException ex) {
			Logging.info("Could not click on  external account service provider input box");
			throw ex;
		}
	}

	/**
	 * <b>Author: </b>Vivian Xie<br>
	 * <b>Date: </b>Nov 29, 2016<br>
	 * <b>Description: </b><br>
	 * Select the given protocol option <b>parameter: </b>String option<br>
	 * value can be IMAP, POP
	 */
	public void clickServiceproviderOptionOfExternalAccount(String option) {
		Logging.info("Starting to input external account service provider option:" + option);
		String settings_external_account_service_provider_option = PropertyHelper.coreSettings
				.getPropertyValue("settings_external_account_service_provider_option").replace("+variable+", option);
		Logging.info(settings_external_account_service_provider_option);
		try {
			Base.base.getDriver().findElement(By.xpath(settings_external_account_service_provider_option)).click();
		} catch (WebDriverException ex) {
			Logging.info("Could not input external account service provider option ");
			throw ex;
		}
	}

	/**
	 * <b>Author: </b>Vivian Xie<br>
	 * <b>Date: </b>Nov 29, 2016<br>
	 * <b>Description: </b><br>
	 * input field on expand account <b>parameter: </b>String fieldname<br>
	 * value can be desc, fromname, username, pwd, inserver-host, inserver-port,
	 * outserver-host, outserver-port <b>parameter: </b>String value:<br>
	 * the string is to be input
	 */
	public void inputFiledOnExternalAccount(String fieldname, String value) {
		Logging.info("Starting to input external account " + fieldname + " with value " + value);
		String settings_external_account_input = PropertyHelper.coreSettings
				.getPropertyValue("settings_external_account_input").replace("+variable+", fieldname);
		try {

			Base.base.getDriver().findElement(By.xpath(settings_external_account_input)).clear();
			Base.base.getDriver().findElement(By.xpath(settings_external_account_input)).sendKeys(value);
		} catch (WebDriverException ex) {
			Logging.info("Could not input external account " + fieldname);
			throw ex;
		}
	}

	/**
	 * <b>Author: </b>Vivian Xie<br>
	 * <b>Date: </b>Nov 29, 2016<br>
	 * <b>Description: </b><br>
	 * check/ uncheked ssl of External Account <b>parameter: </b> String
	 * servertype<br>
	 * value can be inserver and outserver boolean check -- true if check the SSL
	 * false if uncheck the SSL
	 */

	public void clickSSLOfExternalAccount(String servertype, boolean check) {
		Logging.info(
				"Starting to " + (check ? "check" : "uncheck") + " external account " + servertype + " SSL checkbox");
		String settings_external_account_SSL_table = PropertyHelper.coreSettings
				.getPropertyValue("settings_external_account_SSL_checkbox").replace("+variable+", servertype);
		String settings_external_account_SSL_checkbox = PropertyHelper.coreSettings
				.getPropertyValue("settings_external_account_SSL_checkbox").replace("+variable+", servertype);
		boolean checked = false;
		try {
			checked = Base.base.getDriver().findElement(By.xpath(settings_external_account_SSL_table))
					.getAttribute("class").contains("cb-checked");
			Logging.info("class=" + Base.base.getDriver().findElement(By.xpath(settings_external_account_SSL_table))
					.getAttribute("class"));
			if (checked != check) {
				Logging.info("equal");
				Base.base.getDriver().findElement(By.xpath(settings_external_account_SSL_checkbox)).click();
			}
		} catch (WebDriverException ex) {
			Logging.info("Could not click external account  " + servertype + " SSL checkbox ");
			throw ex;
		}
	}

	/**
	 * <b>Author: </b>Vivian Xie<br>
	 * <b>Date: </b>Nov 29, 2016<br>
	 * <b>Description: </b><br>
	 * click button on Adding external account page <b>parameter: </b>String
	 * buttonName:<br>
	 * button name
	 */

	public void clickButtonOnExternalAccountPage(String buttonName) {
		Logging.info("Starting to click button " + buttonName + "  on exteral account page");
		String settings_external_accoount_add_page_button = PropertyHelper.coreSettings
				.getPropertyValue("settings_external_accoount_add_page_button").replace("+variable+", buttonName);
		try {

			Base.base.getDriver().findElement(By.xpath(settings_external_accoount_add_page_button)).click();
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.info("Could not click button " + buttonName + " on exteral account page");
			throw ex;
		}
	}

	/**
	 * <b>Author: </b>Vivian Xie<br>
	 * <b>Date: </b>Nov 29, 2016<br>
	 * <b>Description: </b><br>
	 * expand / fold Advanced Settings <b>parameter: </b>boolean open<br>
	 * true if expand Advanced Settings, false if fold Advanced Settings
	 * 
	 */
	public void expandAdvancedSettingsOfExternalAccont(boolean open) {
		Logging.info("start to " + (open ? "expand" : "hide") + "expand advances setting of external account: ");
		boolean expanded = false;
		String labeltext = null;
		String settings_external_account_advanced_settings_label = PropertyHelper.coreSettings
				.getPropertyValue("settings_external_account_advanced_settings_label");
		try {
			labeltext = Base.base.getDriver().findElement(By.xpath(settings_external_account_advanced_settings_label))
					.getText();
			if (labeltext.contains("Hide advanced settings"))
				expanded = true;
			if (expanded != open) {
				Base.base.getDriver().findElement(By.xpath(settings_external_account_advanced_settings_label)).click();
			}
		} catch (WebDriverException ex) {
			Logging.info("Could not find Advanced Settings label ");
			throw ex;
		}

	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Dec 14, 2016<br>
	 * <b>Description</b><br>
	 * click on font name of font size combine box . <b>Parameters</b><br>
	 * String font - font style name. One of the following buttons should be
	 * provided: - Font - Font size
	 */
	public void clickFontStyle(String font) {
		Logging.info("Starting to  click  " + font + " combine box  ");
		String email_composer_font_style_button = PropertyHelper.coreSettings
				.getPropertyValue("email_composer_font_style_button").replace("+variable+", font);

		try {
			Base.base.getDriver().findElement(By.xpath(email_composer_font_style_button)).click();

		} catch (WebDriverException ex) {
			Logging.error("Could not click " + font);
			throw ex;
		}

	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Dec 14, 2016<br>
	 * <b>Description</b><br>
	 * click on font name of font size combine box . <b>Parameters</b><br>
	 * String font - font style name. One of the following buttons should be
	 * provided: - Font - Font size String style-- the specific style
	 */
	public void selectFontStyle(String font, String style) {
		Logging.info("Starting to  select" + font + " combine box  ");

		String email_composer_font_style_options = PropertyHelper.coreSettings
				.getPropertyValue("settings_composer_font_style_options").replace("+variable1+", font)
				.replace("+variable2+", style);
		try {
			Base.base.getDriver().findElement(By.xpath(email_composer_font_style_options)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not select " + font + "style: " + style);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Dec 15, 2016<br>
	 * <b>Description</b><br>
	 * Verify if the specific content has the correct font name</b><br>
	 * String fontName -- the expected font name String content -- the content
	 * <b>return:</b>true if the font name is correct ,else false
	 */
	public boolean verifyFontName(String fontName, String content) {
		Logging.info("Starting to verify if content: \" " + content + "\" has font name " + fontName);

		String email_composer_font_name_content = PropertyHelper.coreSettings
				.getPropertyValue("settings_composer_font_name_content").replace("+variable1+", fontName)
				.replace("+variable2+", content);
		Logging.info(email_composer_font_name_content);
		boolean result = false;

		try {
			ArrayList<WebElement> elem = (ArrayList<WebElement>) Base.base.getDriver()
					.findElements(By.xpath(email_composer_font_name_content));
			if (elem.size() > 0)
				result = true;
		} catch (WebDriverException ex) {
		}
		Logging.info("Content: \"" + content + "\" has font name " + fontName + ": " + result);
		return result;
	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Dec 15, 2016<br>
	 * <b>Description</b><br>
	 * Verify if the specific content has the correct font size</b><br>
	 * 
	 * @param size
	 *            -- font size, value can be large small or normal String fontName
	 *            -- the expected font soize String content -- the content
	 *            <b>return:</b>true if the font size is correct ,else false
	 */
	public boolean verifyFontSize(String size, String content) {
		Logging.info("Starting to verify if content: \"" + content + "\" has font size " + size);
		String email_composer_font_size_content = null;

		if (size.trim().equals("normal")) {
			email_composer_font_size_content = PropertyHelper.coreSettings
					.getPropertyValue("settings_composer_font_size_content").replace("+variable1+", size)
					.replace("+variable2+", content);
		} else {
			email_composer_font_size_content = PropertyHelper.coreSettings
					.getPropertyValue("settings_composer_font_size_normal_content").replace("+variable2+", content);
		}

		Logging.info(email_composer_font_size_content);
		boolean result = false;

		try {
			ArrayList<WebElement> elem = (ArrayList<WebElement>) Base.base.getDriver()
					.findElements(By.xpath(email_composer_font_size_content));
			if (elem.size() > 0)
				result = true;
		} catch (WebDriverException ex) {
		}
		Logging.info("Content: \"" + content + "\" has font size " + size + ": " + result);
		return result;
	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Dec 15, 2016<br>
	 * <b>Description</b><br>
	 * Verify if the specific content has the align</b><br>
	 * String align -- the expected align ,value can be "center","right","justify"
	 * String content -- the content <b>return</b> true if the align is correct
	 * ,else false
	 */
	public boolean verifyTextAlign(String align, String content) {
		Logging.info("Starting to verify if content: content \"" + content + "\" has align  " + align);
		String email_composer_text_align_content = null;
		email_composer_text_align_content = PropertyHelper.coreSettings
				.getPropertyValue("settings_composer_text_align_content").replace("+variable1+", String.valueOf(align))
				.replace("+variable2+", content);
		boolean result = false;
		try {
			ArrayList<WebElement> elem = (ArrayList<WebElement>) Base.base.getDriver()
					.findElements(By.xpath(email_composer_text_align_content));
			if (elem.size() > 0)
				result = true;
		} catch (WebDriverException ex) {
		}
		Logging.info("Content: \"" + content + "\" has align " + align + ": " + result);
		return result;
	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Dec 15, 2016<br>
	 * <b>Description</b><br>
	 * Verify if the specific list has the correct type</b><br>
	 * String type -- the expected type ,value can be "bulleted","numbered" String
	 * content -- the content <b>return</b> true if the type is correct ,else false
	 */
	public boolean verifyListTyle(String type, String content) {
		Logging.info("Starting to verify if content: content \"" + content + "\" is  " + type + " list");
		String listType = null;
		if (type.equalsIgnoreCase("bulleted"))
			listType = "ul";
		else
			listType = "ol";
		String email_composer_type_list_content = PropertyHelper.coreSettings
				.getPropertyValue("settings_composer_type_list_content")
				.replace("+variable1+", String.valueOf(listType)).replace("+variable2+", content);
		boolean result = false;
		try {
			ArrayList<WebElement> elem = (ArrayList<WebElement>) Base.base.getDriver()
					.findElements(By.xpath(email_composer_type_list_content));
			if (elem.size() > 0)
				result = true;
		} catch (WebDriverException ex) {
		}
		Logging.info("Content: \"" + content + "\" is " + type + " list : " + result);
		return result;
	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Dec 15, 2016<br>
	 * <b>Description</b><br>
	 * Verify if the specific text has the correct background color</b><br>
	 * Color color -- the expected color String content -- the content <b>return</b>
	 * true if the color is correct ,else false
	 */
	public boolean verifyBackgroundColor(Color color, String content) {
		String colorString = Tools.color2RgbString(color);
		Logging.info("Starting to verify if content: content \"" + content + "\" background  is  " + colorString);
		String email_compose_background_color_content = PropertyHelper.coreSettings
				.getPropertyValue("settings_compose_background_color_content").replace("+variable1+", colorString)
				.replace("+variable2+", content);

		Logging.info(email_compose_background_color_content);
		boolean result = false;
		try {
			ArrayList<WebElement> elem = (ArrayList<WebElement>) Base.base.getDriver()
					.findElements(By.xpath(email_compose_background_color_content));
			if (elem.size() > 0)
				result = true;
		} catch (WebDriverException ex) {
		}
		Logging.info("Content: \"" + content + "\" background color is " + colorString + ":" + result);
		return result;
	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Dec 16, 2016<br>
	 * <b>Description</b><br>
	 * Type in signature rich body content </b><br>
	 * CharSequence key -- the content
	 */
	public void typeSignatureRichBody(CharSequence key) {
		Logging.info("Starting type in signature rich body :" + key);
		String settings_signature_rich_body = PropertyHelper.coreSettings
				.getPropertyValue("settings_signature_rich_body");

		try {
			Base.base.getDriver().findElement(By.xpath(settings_signature_rich_body)).click();
			Base.base.getDriver().findElement(By.xpath(settings_signature_rich_body)).sendKeys(key);

		} catch (WebDriverException ex) {
			Logging.info("Failed to find signature rich body");
			throw ex;
		}

	}

	/**
	 * <b>Author</b>: Charlie Li<br>
	 * <b>Date</b>: August 25, 2017<br>
	 * <b>Description</b><br>
	 * <b>Verify if blocked sender list has the specific itemName</b><br>
	 * <b>return</b> true if blocked sender list has the specific itemName ,else
	 * false
	 */
	public boolean verifyBlockedSenderListItemByEmail(String itemName) {
		itemName = itemName.trim();

		Logging.info("Verify blocked sender existence by email:" + itemName);
		String settings_block_sender_list_item = PropertyHelper.coreSettings
				.getPropertyValue("settings_block_sender_list_item").replace("+variable+", itemName);
		boolean result = false;
		try {
			Base.base.getDriver().findElement(By.xpath(settings_block_sender_list_item));
			result = true;
			Logging.info("Found blocked sender by email: " + itemName);
		} catch (WebDriverException ex) {
			Logging.info("Failed to find blocked sender by email" + itemName);

		}
		return result;

	}

	/**
	 * <b>Author</b>: Charlie Li<br>
	 * <b>Date</b>: August 11, 2015<br>
	 * <b>Description</b><br>
	 * cleanup all block senders in list
	 */

	public void cleanupAllBlockedSenders() {
		Logging.info(" blocked senders in list");
		String settings_blockedSender_list = PropertyHelper.coreSettings.getPropertyValue("settings_blocksender_list");

		try {
			Tools.setDriverDefaultValues();
			List<WebElement> blockSenders = Base.base.getDriver().findElements(By.xpath(settings_blockedSender_list));
			Logging.info(blockSenders.toString());
			if (blockSenders.size() != 0) {
				for (WebElement blockSender : blockSenders) {
					blockSender.click();
					this.clickBlockSenderButton("Delete");
				}
			} else {
				Logging.info("there is no any blocked sender in list");
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not cleanup the signatures");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Charlie Li<br>
	 * <b>Date</b>: August 29, 2017<br>
	 * <b>Description</b><br>
	 * click on a specified Blocked sender buttons (Add/Edit/Delete)
	 * <b>Parameters</b><br>
	 * String button - blocked sender buttons One of the following item should be
	 * provided: - Add - Refresh - Delete
	 */
	public void clickBlockSenderButton(String button) {
		Logging.info("Clicking on button: " + button);
		button = button.toLowerCase();
		String settings_blockedSender_button = PropertyHelper.coreSettings
				.getPropertyValue("settings_blockedSender_button").replace("+variable+", button);

		boolean disabled = false;

		try {
			Tools.setDriverDefaultValues();
			Tools.scrollElementIntoViewByXpath(settings_blockedSender_button);

			disabled = Base.base.getDriver().findElement(By.xpath(settings_blockedSender_button)).getAttribute("class")
					.contains("x-disabled");
			if (!disabled) {
				Base.base.getDriver().findElement(By.xpath(settings_blockedSender_button)).click();
				this.waitForLoadMaskDismissed();
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not click on blockSender Button " + button);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 07, 2015<br>
	 * <b>Description</b><br>
	 * verify signature exists in signature list by name <b>Parameters</b><br>
	 * String name: signature name
	 */
	public boolean verifyAutoReplyEndDateIsValid() {
		Logging.info("Verifying Auto-reply end date is valid: ");
		String settings_auto_reply_end_date_invalid_input = PropertyHelper.coreSettings
				.getPropertyValue("settings_auto_reply_end_date_invalid_input");
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(settings_auto_reply_end_date_invalid_input));
			Logging.info("Auto-reply end date is invalid ");
		} catch (WebDriverException ex) {
			result = true;
			Logging.info("Auto-reply end date is valid");
		}
		return result;
	}

	/**
	 * <b>Author: </b>Charlie Li<br>
	 * <b>Date: </b>September 05, 2017<br>
	 * <b>Description: </b><br>
	 * Selects today from a floating date picker. Applicable to all floating date
	 * pickers in Settings.
	 */
	public void selectTodayInFloatingDatePicker() {
		Logging.info("Selecting today in floating date picker: ");
		String settings_auto_reply_date_today = PropertyHelper.coreSettings
				.getPropertyValue("settings_auto_reply_date_today");
		try {
			Tools.waitFor(2, TimeUnit.SECONDS);

			Base.base.getDriver().findElement(By.xpath(settings_auto_reply_date_today)).click();
		} catch (WebDriverException ex) {
			Logging.warn("Could not select today in floating date picker: ");
		}
	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: August 30, 2017<br>
	 * <b>Description</b><br>
	 * Remove the association related to the cloud account
	 */
	public void clickRemoveCloudAssociationButton() {

		Logging.info("Click the remove cloud assiontion button");
		String settings_could_remove_association = PropertyHelper.coreSettings
				.getPropertyValue("settings_could_remove_association");
		String settings_could_add_association = PropertyHelper.coreSettings
				.getPropertyValue("settings_could_add_association");

		try {
			Base.base.getDriver().findElement(By.xpath(settings_could_remove_association)).click();
			Tools.waitUntilElementDisplayedByXpath(settings_could_add_association, 20);
		} catch (WebDriverException ex) {
			Logging.error("Fail to click remove cloud association");

		}

	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: August 30, 2017<br>
	 * <b>Description</b><br>
	 * Add the association related to the cloud account
	 */
	public void clickAddCloudAssociation() {
		Logging.info("Click the add cloud association button");
		String settings_could_add_association = PropertyHelper.coreSettings
				.getPropertyValue("settings_could_add_association");
		try {
			Base.base.getDriver().findElement(By.xpath(settings_could_add_association)).click();
		} catch (WebDriverException ex) {
			Logging.error("Fail to click add cloud association");

		}
	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: August 30, 2017<br>
	 * <b>Description</b><br>
	 * Add the personal account <b>Parameters</b><br>
	 * 
	 * @param personalCouldUserName
	 *            - the user name of the personal cloud
	 * @param personalCouldPwd
	 *            - the password of the personal cloud
	 */
	public void addPersonCouldAccount(String personalCouldUserName, String personalCouldPwd) {
		Logging.info("Click the add cloud association button, the user name is :" + personalCouldUserName
				+ ", and password is " + personalCouldPwd);
		String settings_cloud_page_user_name = PropertyHelper.coreSettings
				.getPropertyValue("settings_cloud_page_user_name");
		String settings_cloud_page_passwd = PropertyHelper.coreSettings.getPropertyValue("settings_cloud_page_passwd");
		String settings_cloud_page_login_button = PropertyHelper.coreSettings
				.getPropertyValue("settings_cloud_page_login_button");
		String settings_item = PropertyHelper.coreSettings.getPropertyValue("settings_item").replace("+variable+",
				"Cloud");

		try {
			Tools.waitUntilElementDisplayedByXpath(settings_cloud_page_user_name, 15);
			Base.base.getDriver().findElement(By.xpath(settings_cloud_page_user_name)).clear();
			Base.base.getDriver().findElement(By.xpath(settings_cloud_page_user_name)).sendKeys(personalCouldUserName);
			Base.base.getDriver().findElement(By.xpath(settings_cloud_page_passwd)).clear();
			Base.base.getDriver().findElement(By.xpath(settings_cloud_page_passwd)).sendKeys(personalCouldPwd);
			Base.base.getDriver().findElement(By.xpath(settings_cloud_page_login_button)).click();

			Tools.waitUntilElementDisplayedByXpath(settings_item, 30);

		} catch (WebDriverException ex) {
			Logging.error("Fail to add personal cloud account");

		}
		Logging.info("Add personal cloud account is done ");

	}

	/**
	 * <b>Author</b>: Charlie Li<br>
	 * <b>Date</b>: Oct 12, 2017<br>
	 * <b>Description</b><br>
	 * Verify the email address is invalid in the "ForwadToEmail" field
	 * <b>Parameters</b><br>
	 * 
	 * @param email
	 *            - the email address
	 */
	public boolean verifyRuleEmailInputIsInvalid(String email) {
		Logging.info("Verifying the email is invalid: ");
		String settings_message_filters_email_invalid_input = PropertyHelper.coreSettings
				.getPropertyValue("settings_message_filters_email_invalid_input").replace("+variable+", email);
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(settings_message_filters_email_invalid_input));
			result = true;
			Logging.info("The email is invalid ");
		} catch (WebDriverException ex) {
			Logging.info("The email is invalid");
		}
		return result;
	}

	/**
	 * <b>Author</b>: Joey Fang<br>
	 * <b>Date</b>: Oct 16, 2017<br>
	 * <b>Description</b><br>
	 * check "White List" button <b>Parameters</b><br>
	 * boolean check: true - to enable / false - to disable
	 */
	public void checkWhiteList(boolean check) {
		Logging.info("check \"Only allow mail from known senders\" as: " + (check ? "enable" : "disable"));

		String settings_allow_whitelist_enable_table = PropertyHelper.coreSettings
				.getPropertyValue("settings_allow_whitelist_enable_table");
		String settings_allow_whitelist_enable_button = PropertyHelper.coreSettings
				.getPropertyValue("settings_allow_whitelist_enable_button");

		try {
			Tools.setDriverDefaultValues();

			// Set enable or disable.
			boolean isEnabled = Base.base.getDriver().findElement(By.xpath(settings_allow_whitelist_enable_table))
					.getAttribute("class").contains("cb-checked");

			if ((check && !isEnabled) || (!check && isEnabled)) {
				Base.base.getDriver().findElement(By.xpath(settings_allow_whitelist_enable_button)).click();
			}

		} catch (WebDriverException ex) {
			Logging.error(
					"Could not check \"Only allow mail from known senders\" as: " + (check ? "enable" : "disable"));
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Joey Fang<br>
	 * <b>Date</b>: Oct 16, 2017<br>
	 * <b>Description</b><br>
	 * verify if WhiteList is checked or not <b>Parameters</b><br>
	 * 
	 */
	public boolean verifyWhiteListCheckOrNot() {
		Logging.info("Verifying WhiteList is: enable");

		String settings_allow_whitelist_enable_table = PropertyHelper.coreSettings
				.getPropertyValue("settings_auto_forward_enable_table");

		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(settings_allow_whitelist_enable_table)).getAttribute("class")
					.contains("cb-checked");
			// signature is found.
			result = true;
			Logging.info("WhiteList is enabled: " + result);
		} catch (WebDriverException ex) {
			Logging.info("WhiteList is not enabled: " + result);

		}

		return result;
	}

	/**
	 * <b>Author</b>: Joey Fang<br>
	 * <b>Date</b>: Oct 25, 2017<br>
	 * <b>Description</b><br>
	 * verify contact exists in Safe Sender/Block Sender list <b>Parameters</b><br>
	 * String name: email address name
	 */
	public boolean verifyAddressInSafeList(String address) {
		Logging.info("Verifying email address existence in list: " + address);
		address = address.trim();
		String settings_contact_emailaddress_safe_block_sender = PropertyHelper.coreSettings
				.getPropertyValue("settings_contact_emailaddress_safe_block_sender").replace("+variable+", address);
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(settings_contact_emailaddress_safe_block_sender));

			result = true;
			Logging.info("Found contact email address by name: " + address);
		} catch (WebDriverException ex) {
			Logging.info("Could not found contact email address by name: " + address);
		}
		return result;
	}

	/**
	 * <b>Author</b>: Joey Fang<br>
	 * <b>Date</b>: Oct 28, 2017<br>
	 * <b>Description</b><br>
	 * Get the domain for the emailaddress <b>Parameters</b><br>
	 * 
	 */
	public String getEmailDomain(String email) {
		Logging.info("Get the email domain for: " + email);
		String domain = null;

		try {
			String[] emailSplit = email.split("@");
			for (int i = 0; i < emailSplit.length; i++) {
				domain = emailSplit[1];
			}
			Logging.info("Domain is: " + domain);
		} catch (WebDriverException ex) {
			Logging.info("Split domain failed");

		}

		return domain;
	}

	/**
	 * <b>Author</b>: Joey Fang<br>
	 * <b>Date</b>: Jan 22, 2018<br>
	 * <b>Description</b><br>
	 * Get the user profile name from Settings <b>Parameters</b><br>
	 * 
	 */
	public String getProfileName() {
		Logging.info("Get the user profile name");
		String firstName;
		String lastName;
		String profileName = null;

		try {

			String settings_profile_firstname = PropertyHelper.coreSettings
					.getPropertyValue("settings_profile_firstname");
			String settings_profile_lastname = PropertyHelper.coreSettings
					.getPropertyValue("settings_profile_lastname");

			firstName = Base.base.getDriver().findElement(By.xpath(settings_profile_firstname)).getAttribute("value");
			lastName = Base.base.getDriver().findElement(By.xpath(settings_profile_lastname)).getAttribute("value");
			profileName = firstName + " " + lastName;
			Logging.info("Profile Name is: " + profileName);

		} catch (WebDriverException ex) {
			Logging.info("Get Profile Name Failed");

		}

		return profileName;
	}

}
