package com.synchronoss.core;

/**
 * Object representations of web element name-value mappings.
 */
public class PropertyHelper {
	
    private static String userInterface;
    
    public static String getUserInterface() {
        return userInterface;
    }
    
    public static void setUserInterface(String ui) {
        userInterface = ui;
    }
    
    public static PropertyFiles pageObjectBaseFile = new PropertyFiles("PageObjectBase");
    public static PropertyFiles coreLoginFile = new PropertyFiles("CoreLogin");
    public static PropertyFiles coreNavigationFile = new PropertyFiles("CoreNavigation");
    public static PropertyFiles coreEmailFile = new PropertyFiles("CoreEmail");
    public static PropertyFiles coreEmailComposer = new PropertyFiles("CoreEmailComposer");
    public static PropertyFiles coreContacts = new PropertyFiles("CoreContacts");
    public static PropertyFiles coreCalendar = new PropertyFiles("CoreCalendar");
    public static PropertyFiles coreSettings = new PropertyFiles("CoreSettings");
    public static PropertyFiles CoreTasks = new PropertyFiles("CoreTasks");
    public static PropertyFiles Utility = new PropertyFiles("Utility");
    public static PropertyFiles HubConfig = new PropertyFiles("HubConfig");
    public static PropertyFiles configParams = new PropertyFiles("Config");
}
