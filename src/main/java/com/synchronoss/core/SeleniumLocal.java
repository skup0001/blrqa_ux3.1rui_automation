package com.synchronoss.core;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;

/**
 * Creates WebDriver instances to execute tests on local machines.
 */
public class SeleniumLocal extends Base {
	
	public static WebDriver driver = null;
	
	
	/*************************************************************************
	 * @Method Name : getDriver
	 * @Author : Alkesh Dagade
	 * @Created On : 08/29/2012
	 * @Description : This method is to get WebDriver instance.
	 * @parameter 1 : NA
	 *************************************************************************/
	public WebDriver getDriver() {
		return SeleniumLocal.driver;
	}
	
	
	/*************************************************************************
	 * @Method Name : SeleniumLocal (constructor)
	 * @Author : Alkesh Dagade
	 * @Created On : 08/29/2012
	 * @Description : This method is to create WebDriver and Selenium instance
	 *              as per browser type. 
	 * @parameter 1 : browser: Browser name on which suite to be possible values
	 *            are: Firefox, Chrome and internet explorer
	 * @parameter 2 : url: application URL on which suite to be executed
	 *************************************************************************/
	public SeleniumLocal(String browser) {
		
		if (browser.equalsIgnoreCase("firefox")) {
			driver = new FirefoxDriver();
		} else if (browser.equalsIgnoreCase("chrome")) {
			//System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\drivers\\chromedriver.exe");
			//System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\drivers\\chromedriver.exe");
			//System.setProperty("webdriver.chrome.driver","C:\\Softwares\\chromedriver.exe");
			System.setProperty("webdriver.chrome.driver","C:\\Users\\skup0001\\Desktop\\Supreeth\\Softwares\\chromedriver_win32\\chromedriver.exe");
			driver = new ChromeDriver();
		} else if (browser.equalsIgnoreCase("safari")) {
			driver = new SafariDriver();
		} else {
			driver = new InternetExplorerDriver();
		}
	}
	
	
	@Override
	protected void startSession(String url) {
		Logging.info("Navigating to: " + url);
		// Navigating to test site.
		SeleniumLocal.driver.get(url);
	}
	
}

