package com.synchronoss.core;

import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 * Customized version of Remote Webdriver. Only use this version to execute test 
 * on Microsoft Edge, which used the customized version of WebElement - 
 * "WebElementForEdge".
 * 
 * Executing the current tests on Microsoft Edge encountered an unresolved issue of 
 * the browser itself (or the MS Webdriver Server), causing clicking actions to throw 
 * an exception of "Element is Obscured". This has been tracked on Microsoft forum: 
 * https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/5238133/
 * 
 * @author Jerry Zhang
 */
public class RemoteWebDriverForEdge extends RemoteWebDriver {

	public RemoteWebDriverForEdge(URL remoteAddress, Capabilities desiredCapabilities) {
		super(remoteAddress, desiredCapabilities);
	}

	@Override
	public WebElement findElement(By by) {
		return new WebElementForEdge(super.findElement(by));
	}
}
