package com.synchronoss.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;

/**
 * Utility class helps maintain retry counts in a local properties file, and does folder
 * manipulations to backup previous test results when required.
 * 
 * @author Jerry Zhang
 */
public class RetryHelper {
	
	private static String rootPath = System.getProperty("user.dir");
	private static String helperPropertiesPath = rootPath + File.separator 
			+ "retry_helper.properties";
	private static String testOutputDir = rootPath + File.separator 
			+ "test-output";
	private static String screenshotsDir = rootPath + File.separator 
			+ "screenshots";
	private static String testOutputBackupDir = rootPath + File.separator 
			+ "test-output-backup";
	
	private static final String RETRY_FLAG = "executeAsRetry";
	private static final String MAX_RETRY_COUNT = "maxRetryCount";
	private static final String CURRENT_RETRY_COUNT = "currentRetryCount";
	
	/**
	 * Writes retry count to a local properties file.
	 * @param executeAsRetry
	 * @param maxRetryCount
	 * @param currentRetryCount
	 * @throws IOException 
	 */
	public static void updateRetryProperties(String executeAsRetry, 
			String maxRetryCount, String currentRetryCount) throws IOException {
		
		FileOutputStream fileOut = null;
		
		try {
			Properties properties = new Properties();
			
			properties.setProperty(RETRY_FLAG, executeAsRetry);
			properties.setProperty(MAX_RETRY_COUNT, maxRetryCount);
			properties.setProperty(CURRENT_RETRY_COUNT, currentRetryCount);
			
			fileOut = new FileOutputStream(new File(helperPropertiesPath));
			properties.store(fileOut, "Temporary properties for retry-execution. "
					+ "DO NOT modify this file manually.");
		} finally {
			if (fileOut != null) {
				fileOut.close();
			}
		}
	}
	
	/**
	 * Fetches retry properties that was saved on the local properties file.
	 * @param key
	 * @throws IOException 
	 */
	private static String fetchRetryProperties(String key) throws IOException {
		Properties properties = new Properties();
		
		FileInputStream fileIn = null;
		
		try {
			fileIn = new FileInputStream(new File(helperPropertiesPath));
			properties.load(fileIn);
			fileIn.close();
		} finally {
			if (fileIn != null) {
				fileIn.close();
			}
		}
		
		return properties.getProperty(key);
	}
	
	public static String getRetryFlag() throws IOException {
		return fetchRetryProperties(RETRY_FLAG);
	}
	
	public static String getMaxRetryCount() throws IOException {
		return fetchRetryProperties(MAX_RETRY_COUNT);
	}
	
	public static String getCurrentRetryCount() throws IOException {
		return fetchRetryProperties(CURRENT_RETRY_COUNT);
	}
	
	/**
	 * Does backup work of previous test results: 
	 * 1. Rename existing test-output/screenshots folders with distinct names (by adding 
	 * a trailing part); 
	 * 2. Move the renamed test-output/screenshots folders to a backup folder.
	 * 
	 * @param folderNameTrailingPart
	 */
	public static void backupTestResults(String folderNameTrailingPart) {
		File testOutputDirFile = new File(testOutputDir);
		File renamedTestOutputDirFile = new File(testOutputDir + folderNameTrailingPart);
		File screenshotsDirFile = new File(screenshotsDir);
		File renamedScreenshotsDirFile = new File(screenshotsDir + folderNameTrailingPart);
		File testOutputBackupDirFile = new File(testOutputBackupDir);
		
		// Rename test-output and screenshots folders.
		try {
			FileUtils.moveDirectory(testOutputDirFile, renamedTestOutputDirFile);
			Logging.info("Renamed test result folder, from <" + testOutputDirFile 
					+ "> to <" + renamedTestOutputDirFile + ">");
			
			FileUtils.moveDirectory(screenshotsDirFile, renamedScreenshotsDirFile);
			Logging.info("Renamed screenshots folder, from <" + screenshotsDirFile 
					+ "> to <" + renamedScreenshotsDirFile + ">");
		} catch (IOException e) {
			Logging.error("Fail to rename original test-output/screenshots folder, error: " 
					+ e.getMessage());
		}
		// Backup test-output and screenshots folder.
		try {
			FileUtils.moveDirectoryToDirectory(renamedTestOutputDirFile, 
					testOutputBackupDirFile, true);
			Logging.info("Moved test result folder <" + renamedTestOutputDirFile 
					+"> into backup directory <" + testOutputBackupDirFile + ">");
			
			FileUtils.moveDirectoryToDirectory(renamedScreenshotsDirFile, 
					testOutputBackupDirFile, true);
			Logging.info("Moved screenshots folder <" + renamedScreenshotsDirFile 
					+"> into backup directory <" + testOutputBackupDirFile + ">");
		} catch (IOException e) {
			Logging.error("Fail to move test-output/screenshots folder, error: " 
					+ e.getMessage());
		}

	}
}
