package com.synchronoss.core;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

/**
 * Contains framework-wise utility methods.
 */
public class Tools {

	public static void captureScreen(String imageName) {

		final String dir = System.getProperty("user.dir");

		String directory = dir + "/screenshots/";

		System.out.println("[SCREENSHOT] Saving image to: " + directory);

		Reporter.log("[SCREENSHOT] Captured with name: " + imageName + ".png");
		// Logging.info("[SCREENSHOT] Captured with name: " + "<a
		// href=\""+directory+imageName+".png\">"+imageName + ".png</a>");
		// Reporter.log("[SCREENSHOT] Captured with name: " + "<a
		// href=\""+directory+imageName+".png\">"+imageName + ".png</a>");
		System.out.println("[SCREENSHOT] Captured with name: " + imageName + ".png");
		WebDriver augmentedDriver = new Augmenter().augment(Base.base.getDriver());
		File srcFile = ((TakesScreenshot) augmentedDriver).getScreenshotAs(OutputType.FILE);
		try {
			// Remove existing pictures
			FileUtils.copyFile(srcFile, new File(directory + imageName + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * <b>Author</b>: Jerry Zhang <br>
	 * <b>Date</b>: Jun 02, 2015 <br>
	 * <b>Description</b><br>
	 * set the following WebDriver values to be default:<br>
	 * - focused content of the locator<br>
	 * - implicit wait timeout<br>
	 * 
	 * @return void
	 */
	public static void setDriverDefaultValues() {
		try {
			Base.base.getDriver().switchTo().defaultContent();
			Base.base.getDriver().manage().timeouts().implicitlyWait(Long.parseLong(Base.timeout), TimeUnit.SECONDS);
		} catch (WebDriverException ex) {
			Logging.warn("Fail to set WebDriver to default values");
		}
	}

	/**
	 * Waits until the given period of time has been expired and then continue
	 * normal execution. The method will explicitly keep Selenium to wait without
	 * any kind of shortcut to continue if some element becomes available. In
	 * addition for that behavior Thread.sleep() is explicitly not used due to
	 * timeout issues for longer periods.
	 */
	public static void waitFor(long period, TimeUnit unit) {
		Logging.info("Time before driver wait: " + period + " " + unit + ". started on " + (new Date()).toString());

		long timeout = unit.toSeconds(period);

		Base.base.getDriver().manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);

		try {
			Base.base.getDriver().findElement(By.xpath("//not-existing-element-" + (new Date()).getTime()));
		} catch (NoSuchElementException nsee) {
			Logging.info("Time after driver wait: " + (new Date()).toString());
		} finally {
			Base.base.getDriver().manage().timeouts().implicitlyWait(Long.parseLong(Base.timeout), TimeUnit.SECONDS);
		}
	}

	/**
	 * @Method Name : waitUntilElementDisplayedByXpath
	 * @Author : Jerry Zhang
	 * @Created on : 09/03/2014
	 * @Description : This method will detect and wait for the expected element to
	 *              be displayed in view with the specified timeout value.
	 * @Parameter1 : String element - XPath expression of the element
	 * @Parameter2 : int timeoutInSeconds - timeout value in seconds
	 * @Return: boolean - true: element displayed / false: element not displayed
	 */
	public static boolean waitUntilElementDisplayedByXpath(String element, int timeoutInSeconds) {
		// Set WebDriver's wait time to 1 second. So each iteration will wait for at
		// most 1 second.
		Base.base.getDriver().manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

		boolean isDisplayed = false;

		try {

			for (int i = 0; i < timeoutInSeconds; i++) {
				try {

					WebElement elem = Base.base.getDriver().findElement(By.xpath(element));
					// Element is found, check its visibility.
					if (elem.isDisplayed()) {
						isDisplayed = true;
						break;
					} else {
						Thread.sleep(1000);
					}

				} catch (NoSuchElementException ex) {
					// Element not found, go to next iteration.
				} catch (StaleElementReferenceException ex) {
					// Element not attached to DOM any more, leave for checking in next iteration.

					// Sleep for 1 second in case wait was not performed in previous code.
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			if (!isDisplayed) {
				Logging.warn("Element still not displayed after " + timeoutInSeconds + " seconds, XPath: " + element);
			}

		} catch (WebDriverException ex) {
			// Catch all WebDriver exceptions.
			throw ex;
		} finally {
			// Reset WebDriver's wait time to default.
			Base.base.getDriver().manage().timeouts().implicitlyWait(Long.parseLong(Base.timeout), TimeUnit.SECONDS);
		}

		return isDisplayed;
	}

	// public static boolean waitUntilElementDisplayedByXpath(String element, int
	// timeoutInSeconds) {
	//
	// boolean isDisplayed = false;
	//
	// try {
	//
	// // WebDriverWait wait = new WebDriverWait(Base.base.getDriver(),
	// // timeoutInSeconds);
	// //
	// wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(element)));
	// // isDisplayed = true;
	//
	// isDisplayed = (new WebDriverWait(Base.base.getDriver(), timeoutInSeconds))
	// .until(new ExpectedCondition<Boolean>() {
	// @Override
	// public Boolean apply(WebDriver d) {
	// return d.findElement(By.xpath(element)).isDisplayed();
	// }
	// });
	// //Base.base.getDriver().manage().timeouts().implicitlyWait(Long.parseLong(Base.timeout),
	// TimeUnit.SECONDS);
	//
	// } catch (WebDriverException ex) {
	//
	// throw ex;
	// }
	//
	// return isDisplayed;
	// }

	/**
	 * @Method Name : waitUntilElementNotDisplayedByXpath
	 * @Author : Jerry Zhang
	 * @Created on : 09/03/2014
	 * @Description : This method will detect and wait for the expected element to
	 *              be NOT displayed in view for a specific timeout value.
	 * @Parameter1 : String element - XPath expression of the element
	 * @Parameter2 : int timeoutInSeconds - timeout value in seconds
	 * @Return: boolean - true: element displayed / false: element not displayed
	 */
	public static boolean waitUntilElementNotDisplayedByXpath(String element, int timeoutInSeconds) {
		// Set WebDriver's wait time to 1 seconds to speed up unnecessary waiting.
		Base.base.getDriver().manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

		boolean isDisplayed = true;

		try {
			for (int i = 0; i < timeoutInSeconds; i++) {
				Thread.sleep(1000);

				try {
					if (!Base.base.getDriver().findElement(By.xpath(element)).isDisplayed()) {
						isDisplayed = false;
						break;
					}
				} catch (StaleElementReferenceException ex) {
					// Element not attached to DOM any more, so check in next iteration.
				}
			}

			if (isDisplayed) {
				Logging.warn("Element still displayed after " + timeoutInSeconds + " seconds, XPath: " + element);
			}

		} catch (NoSuchElementException ex) {
			// Element not found.
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			// Reset WebDriver's wait time to default.
			Base.base.getDriver().manage().timeouts().implicitlyWait(Long.parseLong(Base.timeout), TimeUnit.SECONDS);
		}

		return isDisplayed;
	}

	/**
	 * @Method Name : scrollElementIntoViewByXpath
	 * @Author : Jerry Zhang
	 * @Created on : 07/28/2014
	 * @Description : This method will scroll the specified web element into view by
	 *              executing javascript code.
	 * @Parameter : String element - XPath expression of the element
	 */
	public static void scrollElementIntoViewByXpath(String element) {
		try {
			int i = 0;
			while (i < 5) {
				try {
					WebElement destElement = Base.base.getDriver().findElement(By.xpath(element));
					((JavascriptExecutor) Base.base.getDriver()).executeScript("arguments[0].scrollIntoView(true);",
							destElement);
					// To quit loop.
					i = 5;
				} catch (StaleElementReferenceException ex) {
					i += 1;
				}
				// Wait for 500 milliseconds to let the view update.
				Thread.sleep(500);
			}

		} catch (NoSuchElementException ex) {
			Logging.error("Could not scroll element into view; element not found with xpath: " + element);
			// Do not throw exception here.
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * <b>Author</b>: Jerry Zhang<br>
	 * <b>Date</b>: Jun 08, 2015<br>
	 * <b>Description</b><br>
	 * Moves WebDriver focus to the specified web element.
	 * 
	 * @param elementXpath
	 *            Xpath value of the element
	 * @return void
	 */
	public static void moveFocusToElementByXpath(String elementXpath) {
		try {
			Tools.setDriverDefaultValues();
			Tools.scrollElementIntoViewByXpath(elementXpath);

			WebElement element = Base.base.getDriver().findElement(By.xpath(elementXpath));
			Action moveTo = new Actions(Base.base.getDriver()).moveToElement(element).build();
			//Actions moveTo = new Actions(Base.base.getDriver()).moveToElement(element).click(element);
			moveTo.perform();

		} catch (WebDriverException ex) {
			Logging.warn("Could not move focus to element: " + elementXpath);
		}
	}

	/**
	 * <b>Author</b>: Young ZHao<br>
	 * <b>Date</b>: Nov 06, 2015<br>
	 * <b>Description</b><br>
	 * Generate a new string, once the original string in Xpath contains single or
	 * double quotation marks
	 * 
	 * @param elementXpath
	 *            Xpath value of the element
	 * @return new Xpath
	 */
	public static String generateConcatForXPath(String xpath) {
		String returnString = "concat('";
		String postfixString = "')";
		String origString = xpath;
		int length = origString.length();

		try {
			if (origString.contains("'") || origString.contains("\"")) {
				for (int i = 0; i < origString.length(); i++) {
					String subStr = origString.substring(i, i + 1);
					if (subStr.equals("'")) {
						if (i == 0) {
							subStr = "\"'\", '";
							returnString = "concat(";
						} else if (i == length - 1) {
							subStr = ", \"'\"";
							postfixString = ")";
						} else {
							subStr = "', \"'\", '";
						}
					} else if (subStr.equals("\"")) {
						if (i == 0) {
							subStr = "'\"', '";
							returnString = "concat(";
						} else if (i == length - 1) {
							subStr = ", '\"'";
							postfixString = ")";
						} else {
							subStr = "', '\"', '";
						}
					}
					returnString += subStr;
				}
				returnString += postfixString;
			} else {
				returnString = origString;
			}
		} catch (Exception ex) {
			Logging.error("Could not generate concat format for xpath");
		}
		return returnString;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Nov 09, 2015<br>
	 * <b>Description</b><br>
	 * Right click on the specified web element.
	 * 
	 * @param elementXpath
	 *            Xpath value of the element
	 * @return void
	 */
	public static void contextClickOnElementByXpath(String elementXpath) {
		try {
			Tools.setDriverDefaultValues();
			Tools.scrollElementIntoViewByXpath(elementXpath);
			WebElement element = Base.base.getDriver().findElement(By.xpath(elementXpath));
			Action contextClick = new Actions(Base.base.getDriver()).contextClick(element).build();
			contextClick.perform();
		} catch (WebDriverException ex) {
			Logging.error("Could not right click on element: " + elementXpath);
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Nov 09, 2015<br>
	 * <b>Description</b><br>
	 * Drag and drop source web element to target by xpath.
	 * 
	 * @param sourceXpath
	 *            Xpath value of the element
	 * @return void
	 */
	public static void dragAndDropSourceToTargetByXpath(String sourceXpath, String targetXpath) {
		try {
			Tools.setDriverDefaultValues();
			Tools.scrollElementIntoViewByXpath(sourceXpath);
			WebElement sourceElement = Base.base.getDriver().findElement(By.xpath(sourceXpath));
			WebElement targetElement = Base.base.getDriver().findElement(By.xpath(targetXpath));
			Action dragAndDrop = new Actions(Base.base.getDriver()).dragAndDrop(sourceElement, targetElement).build();
			dragAndDrop.perform();
		} catch (WebDriverException ex) {
			Logging.error("Could not drag and drop source: " + sourceXpath + " to target: " + targetXpath);
		}
	}

	/***************************************************************************
	 * @Method Name: captureFullStackTracek
	 * @Author : Ravid Te
	 * @Created : 11/17/13
	 * @Description : This method will help provide full stacktrace report within
	 *              the testlink report
	 * @parameter 1 : NoSuchElementException object
	 *
	 ***************************************************************************/
	public static void captureFullStackTrace(WebDriverException ex) {
		StringWriter errors = new StringWriter();
		ex.printStackTrace(new PrintWriter(errors));
		Assert.fail(errors.toString());
	}

	/**
	 * <b>Author</b>: Jerry Zhang<br>
	 * <b>Date</b>: Aug 07, 2015<br>
	 * <b>Description</b><br>
	 * transfer color to RGB string <b>Parameters</b><br>
	 * Color color
	 */
	public static String color2RgbString(Color color) {
		String r = String.valueOf(color.getRed());
		String g = String.valueOf(color.getGreen());
		String b = String.valueOf(color.getBlue());
		// return r + ", " + g + ", " + b;
		return "rgb(" + r + ", " + g + ", " + b + ")";
	}

	/**
	 * <b>Author</b>: Jerry Zhang<br>
	 * <b>Date</b>: Aug 07, 2015<br>
	 * <b>Description</b><br>
	 * transfer color to hex string <b>Parameters</b><br>
	 * Color color
	 */
	public static String Color2HexString(Color color) {
		String R = Integer.toHexString(color.getRed());
		R = R.length() < 2 ? ('0' + R) : R;
		String B = Integer.toHexString(color.getBlue());
		B = B.length() < 2 ? ('0' + B) : B;
		String G = Integer.toHexString(color.getGreen());
		G = G.length() < 2 ? ('0' + G) : G;
		return '#' + R + B + G;
	}

	/**
	 * Check if a string contains a match for the regular expression
	 * 
	 * @param regExp,
	 *            The regular expression
	 * @param val
	 *            The string to be validated.
	 * @return boolean
	 * 
	 * @author Lily He
	 */

	public static boolean isValidFormat(String regExp, String val) {
		Pattern pattern = Pattern.compile(regExp);
		Matcher matcher = pattern.matcher(val);
		return matcher.matches();
	}

	/**
	 * Check if the current browser is firefox
	 * 
	 * @return boolean
	 */
	public static boolean isFirefox() {
		return Base.getBrowserName().equalsIgnoreCase("firefox");
	}

	/**
	 * Check if the current browser is Internet Explore
	 * 
	 * @return boolean
	 */
	public static boolean isIE() {
		return Base.getBrowserName().equalsIgnoreCase("internet explorer");
	}

	/**
	 * Check if the current browser is Chrome
	 * 
	 * @return boolean
	 */
	public static boolean isChrome() {
		return Base.getBrowserName().equalsIgnoreCase("chrome");
	}

	/**
	 * Get the MD5 of the file
	 * 
	 * @param folderName
	 *            - the folder location
	 * @param fileNames
	 *            - the file name
	 * 
	 * @return String - the MD5 value of the file
	 */
	public static String getMd5ForFile(File folderName, String fileNames) {
		String md5FileValue = null;
		FileInputStream fileNameInputStream = null;
		try {
			isFolderEmpty(folderName);
			// if (isFolderEmpty(folderName))
			Thread.sleep(10000);
			fileNameInputStream = new FileInputStream(folderName + "\\" + fileNames);
			md5FileValue = DigestUtils.md5Hex(IOUtils.toByteArray(fileNameInputStream));
			Logging.info("Got file md5 as " + md5FileValue);
		} catch (IOException ex) {
			Logging.error("Error occurs when get MD5 of the file: " + ex);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(fileNameInputStream);
		}
		return md5FileValue;
	}

	/**
	 * empty the folder on disk
	 * 
	 * @param folderName
	 *            , the folder path
	 * @return true- the folder is clean up successfully
	 */
	public static boolean emptyFolderOnDisk(File folderName) {
		boolean cleanFolderOnDiskSucessfully = false;
		try {
			FileUtils.cleanDirectory(folderName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cleanFolderOnDiskSucessfully = isFolderEmpty(folderName);
		Logging.info("Empty the folder on disk sucessfully ? " + cleanFolderOnDiskSucessfully);
		return cleanFolderOnDiskSucessfully;
	}

	/***
	 * check if the folder is empty
	 * 
	 * @param folderName
	 * @return true - the folder is empty, false - the folder is not empty
	 */
	public static boolean isFolderEmpty(File folderName) {
		boolean isFolderEmpty = false;
		File[] listOfFiles = folderName.listFiles();
		String currentDir = System.getProperty("user.dir");
		System.out.println("Current dir using System:" + currentDir);

		if (listOfFiles.length == 0)
			isFolderEmpty = true;
		else
			Logging.info(listOfFiles[0].toString());
		Logging.info("Get folder list as " + listOfFiles.length);
		return isFolderEmpty;
	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Nov 11, 2016<br>
	 * <b>Description</b><br>
	 * Click element by selenium action when element.click is not working
	 * 
	 * @param elementXpath
	 *            Xpath value of the element
	 */
	public static void clickOnElementByXpath(String elementXpath) {
		try {
			Logging.info("Click element by actions and the xpath of the element is: " + elementXpath);
			Tools.setDriverDefaultValues();
			Tools.scrollElementIntoViewByXpath(elementXpath);
			WebElement element = Base.base.getDriver().findElement(By.xpath(elementXpath));
			Actions actions = new Actions(Base.base.getDriver());
			actions.moveToElement(element).click().perform();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on element: " + elementXpath);
		}
	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 12, 2016<br>
	 * <b>Description</b><br>
	 * Drag and drop source web element to target offset of X, Y.
	 * 
	 * @param sourceXpath
	 *            Xpath value of the element
	 * @param xOffset
	 *            the offset of X axis
	 * @param yOffset
	 *            the offset of Y axis
	 */
	public static void dragAndDropSourceToTargetByXpath(String sourceXpath, int xOffset, int yOffset) {
		try {
			Tools.setDriverDefaultValues();
			Tools.scrollElementIntoViewByXpath(sourceXpath);
			WebElement sourceElement = Base.base.getDriver().findElement(By.xpath(sourceXpath));
			Action dragAndDrop = new Actions(Base.base.getDriver()).dragAndDropBy(sourceElement, xOffset, yOffset)
					.build();
			dragAndDrop.perform();
		} catch (WebDriverException ex) {
			Logging.error("Could not drag and drop source: " + sourceXpath + " to target x: " + xOffset
					+ " to target y: " + yOffset);
		}
	}

	/**
	 * @Method Name: sendSSHCommand
	 * @Author: Vivian Xie
	 * @Created On: Jan/6/2016
	 * @Description: This method will send a command to remote ssh server.
	 * @param host
	 *            - the ip of the remote server
	 * @param username
	 *            - os username of the server
	 * @param password
	 *            - password to login to the server
	 * @param command
	 *            - the command to run.
	 * @return remote server response string.
	 */
	public static String sendSSHCommand(String host, String username, String password, String command) {
		String result = null;
		Channel channel = null;
		Session session = null;
		try {
			JSch jsch = new JSch();
			session = jsch.getSession(username, host, 22);

			session.setConfig("userauth.gssapi-with-mic", "no");
			session.setConfig("StrictHostKeyChecking", "no");
			

			session.setPassword(password);
			// session.connect(60 * 1000);
			session.connect();

			channel = session.openChannel("exec");
			Logging.info("Running command:" + command);
			((ChannelExec) channel).setCommand(command);
			channel.setInputStream(null);
			((ChannelExec) channel).setErrStream(System.err);
			InputStream in = channel.getInputStream();
			channel.connect();

			byte[] tmp = new byte[1024];
			int time = 0;
			while (time < 10) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					result = result + new String(tmp, 0, i);
					System.out.print(new String(tmp, 0, i));
					time++;
				}
				if (channel.isClosed()) {
					if (in.available() > 0)
						continue;
					System.out.println("exit-status: " + channel.getExitStatus());
					time = 11;
				}
				time++;
				Thread.sleep(1000);
			}

			channel.disconnect();

			session.disconnect();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}
		return result;
	}

	
	
	/**
	 * @Method Name: verifyIfPrecessIsUp
	 * @Author: Vivian Xie
	 * @Created On: Jan/25/2016
	 * @Description: This method will verify a specified process is started.
	 * @param host
	 *            - the ip of the remote server
	 * @param username
	 *            - os username of the server
	 * @param password
	 *            - password to login to the server
	 * @param processString
	 *            - the string that will be used to find the process.
	 */
	public static boolean verifyIfPrecessIsUp(String IP, String username, String password, String processString) {
		boolean result = false;
		String commandFind = null;
		commandFind = "netstat -aon |findstr \"" + processString + "\"";
		String returnValue = sendSSHCommand(IP, username, password, commandFind);
		if (returnValue != null && !returnValue.trim().equals("")) {
			result = true;
		}

		Logging.info("port " + processString + " is up:" + result);
		return result;

	}

	/**
	 * @Method Name: sendSSHCommandToKillProcessByPort
	 * @Author: Vivian Xie
	 * @Created On: Jan/6/2016
	 * @Description: This method will send ssh command to remote server to kill
	 *               specific process.
	 * @param host
	 *            - the ip of the remote server
	 * @param username
	 *            - os username of the server
	 * @param password
	 *            - password to login to the server
	 * @param command
	 *            - the command to run.
	 * @param osType
	 *            - windows or linux
	 */
	public static void sendSSHCommandToKillProcessByPort(String IP, String username, String password,
			String processString, String osType) {
		String commandFind = null;
		String commandKill = null;
		if (osType.equals("windows")) {
			commandFind = "netstat -aon |findstr \"" + processString + "\"";
			commandKill = "tskill  ";
		} else {
			commandFind = "netstat -aop | grep  " + processString;
			commandKill = "kill -9 ";
		}
		String returnValue = sendSSHCommand(IP, username, password, commandFind);
		if (returnValue != null) {
			int n = returnValue.length();
			int smaller = n > 100 ? 100 : n;
			returnValue = returnValue.substring(0, smaller);
			returnValue = removeRedundantSpace(returnValue);
		}

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (returnValue != null && !returnValue.trim().equals(" ")) {
			String[] columes = returnValue.split(" ");
			int n = columes.length;
			Logging.info("Starting to kill the process");
			if (osType.equals("windows"))
				commandKill = commandKill + columes[n - 1];
			else
				commandKill = commandKill + columes[6].split("/")[0];
			sendSSHCommand(IP, username, password, commandKill);
		} else {
			Logging.info("The precess is already stopped");
		}
	}

	/**
	 * @Method Name: sendSSHCommandToKillProcessOnWindows
	 * @Author: Vivian Xie
	 * @Created On: Jan/6/2016
	 * @Description: This method will send ssh command to remote server to kill
	 *               specific process windows.
	 * @param host
	 *            - the ip of the remote server
	 * @param username
	 *            - os username of the server
	 * @param password
	 *            - password to login to the server
	 * @param command
	 *            - the command to run.
	 */
	public static void sendSSHCommandToKillProcessOnWindows(String IP, String username, String password,
			String processString) {
		String commandFind = null;
		String commandKill = null;

		commandFind = "tasklist |findstr \"" + processString + "\"";
		commandKill = "tskill ";

		String returnValue = sendSSHCommand(IP, username, password, commandFind);
		while (returnValue != null && !returnValue.trim().equals("")) {
			returnValue = removeRedundantSpace(returnValue);
			Logging.info(returnValue);
			String[] columes = returnValue.split(" ");
			Logging.info("Starting to kill the process");
			commandKill = commandKill + columes[1];
			sendSSHCommand(IP, username, password, commandKill);

			returnValue = sendSSHCommand(IP, username, password, commandFind);
			commandKill = "tskill ";
		}
	}

	/**
	 * @Method Name: sendSSHCommandToRunCommand
	 * @Author: Vivian Xie
	 * @Created On: Jan/6/2016
	 * @Description: This method will send ssh command to remote server.
	 * @param host
	 *            - the ip of the remote server
	 * @param username
	 *            - os username of the server
	 * @param password
	 *            - password to login to the server
	 * @param ExecutableFileDir
	 *            - the command string.
	 */
	public static String sendSSHCommandToRunCommand(String IP, String username, String password,
			String ExecutableFileDir) {
		String result = null;
		Logging.info("Starting to run command:" + ExecutableFileDir + " on server " + IP);
		String command = ExecutableFileDir;
		result = sendSSHCommand(IP, username, password, command);
		return result;
	}

	/**
	 * @Method Name: resetHub
	 * @Author: Vivian Xie
	 * @Created On: Jan/6/2016
	 * @Description: This method will reset the hub and all the node in the hub.
	 * @param hubname
	 *            - the name of the hub
	 */
	public static void resetHub(String hubname) {
		Logging.info("Start to reset the hub:" + hubname);
		Logging.info("hub_" + hubname + "_ip");
		// System.out.println(PropertyHelper.pageObjectBaseFile);
		String hubIp = PropertyHelper.HubConfig.getPropertyValue("hub_" + hubname + "_ip");
		// String hubIp =
		// PropertyHelper.pageObjectBaseFile.getPropertyValue("common_load_mask");
		Logging.info("hub ip:" + hubIp);
		String hubPort = PropertyHelper.HubConfig.getPropertyValue("hub_" + hubname + "_port");
		Logging.info("hubPort:" + hubPort);
		String hubOsuser = PropertyHelper.HubConfig.getPropertyValue("hub_" + hubname + "_osuser");
		Logging.info("hubOsuser:" + hubOsuser);
		String hubPassword = PropertyHelper.HubConfig.getPropertyValue("hub_" + hubname + "_ospassword");
		Logging.info("hubPassword:" + hubPassword);
		String hubstartBatdir = PropertyHelper.HubConfig.getPropertyValue("hub_" + hubname + "_hub_startBat_dir");
		Logging.info("hubstartBatdir:" + hubstartBatdir);
		String nodeIps = PropertyHelper.HubConfig.getPropertyValue("hub_" + hubname + "_node_ips");
		Logging.info("nodeIps:" + nodeIps.toString());
		String nodeport = PropertyHelper.HubConfig.getPropertyValue("hub_" + hubname + "_node_port");
		Logging.info("nodeport:" + nodeport);
		String nodeOsuser = PropertyHelper.HubConfig.getPropertyValue("hub_" + hubname + "_node_osuser");
		Logging.info("nodeOsuser:" + nodeOsuser);
		String nodeOspassword = PropertyHelper.HubConfig.getPropertyValue("hub_" + hubname + "_node_ospassword");
		Logging.info("nodeOspassword:" + nodeOspassword);
		String nodeStartBatDir = PropertyHelper.HubConfig.getPropertyValue("hub_" + hubname + "_node_startBat_dir");
		Logging.info("nodeStartBatDir:" + nodeStartBatDir);
		String osType = PropertyHelper.HubConfig.getPropertyValue("hub_" + hubname + "_os_type");
		Logging.info("hubOsType:" + osType);
		String[] nodes = nodeIps.split(",");

		// shutdown all then node and kill chromedriver process
		for (int i = 0; i < nodes.length; i++) {
			String ip = nodes[i];
			Logging.info("Starting to shutdown node, node ip is " + ip);
			sendSSHCommandToKillProcessByPort(ip, nodeOsuser, nodeOspassword, "0.0.0.0:" + nodeport, "windows");

			// kill all the chromedriver.exe process
			sendSSHCommandToKillProcessOnWindows(ip, nodeOsuser, nodeOspassword, "chromedriver.exe ");

			// kill all the chrome.exe process
			sendSSHCommandToKillProcessOnWindows(ip, nodeOsuser, nodeOspassword, "chrome.exe ");

			// kill all java.exe process
			sendSSHCommandToKillProcessOnWindows(ip, nodeOsuser, nodeOspassword, "java.exe ");
		}

		// shut down the hub
		sendSSHCommandToKillProcessByPort(hubIp, hubOsuser, hubPassword, hubPort, osType);

		// start the hub
		sendSSHCommandToRunCommand(hubIp, hubOsuser, hubPassword, hubstartBatdir);

		// start all the node
		for (int i = 0; i < nodes.length; i++) {
			String ip = nodes[i];
			Logging.info("Starting to start node, node ip is " + ip);
			boolean nodeIsUp = false;
			int count = 0;
			while (!nodeIsUp && count < 3) {
				sendSSHCommandToRunCommand(ip, nodeOsuser, nodeOspassword, nodeStartBatDir);
				nodeIsUp = verifyIfPrecessIsUp(ip, nodeOsuser, nodeOspassword, "0.0.0.0:" + nodeport);
				count++;
			}
		}
	}

	/**
	 * @Method Name: resetHub
	 * @Author: Vivian Xie
	 * @Created On: Jan/6/2016
	 * @Description: This method will remove the redundant space in String .
	 * @param hubname
	 *            - the name of the hub
	 */
	public static String removeRedundantSpace(String origin) {
		Logging.info("Starting to remove redundant space in string:" + origin);
		String source = origin;
		String result = null;
		boolean goOn = true;
		while (goOn) {
			result = source.replaceAll("  ", " ");
			if (result.equals(source))
				goOn = false;
			source = result;
		}
		Logging.info("The result is " + result);
		return result;
	}

	/**
	 * @Method Name: deleteFile
	 * @Author: Vivian Xie
	 * @Created On: Jan/6/2016
	 * @Description: This method will delete the file by file directory .
	 * @param fileDirectory
	 *            - file file directory
	 */
	public static void deleteFile(String fileDirectory) {
		Logging.info("Starting to delete file:" + fileDirectory);
		File file = new File(fileDirectory);
		if (file.exists())
			file.delete();
	}

	/**
	 * Start the Robotil on the current node
	 * 
	 * @Author Vivian Xie
	 * @Created Mar/2/2017
	 * @param hubName
	 *            -- the current hub name
	 */
	public static void startRobotil(String hubName) {
		Logging.info("Starting to start Robotil on the current node.");
		boolean robotIsUp = false;
		String nodeIp = Base.base.getIpOfNode();
		String nodeOsuser = PropertyHelper.HubConfig.getPropertyValue("hub_" + hubName + "_node_osuser");
		Logging.info("nodeOsuser:" + nodeOsuser);
		String nodeOspassword = PropertyHelper.HubConfig.getPropertyValue("hub_" + hubName + "_node_ospassword");
		int count = 0;
		while (!robotIsUp && count < 3) {
			sendSSHCommandToRunCommand(nodeIp, nodeOsuser, nodeOspassword, "C:/Automation/startRobot.bat");
			robotIsUp = verifyIfPrecessIsUp(nodeIp, nodeOsuser, nodeOspassword, "0.0.0.0:6666");
			count++;
		}

	}

	/**
	 * Stop the Robotil on the current node
	 * 
	 * @Author Vivian Xie
	 * @Created Mar/2/2017
	 * @param hubName
	 *            -- the current hub name
	 */
	public static void stopRobotil(String hubName) {
		Logging.info("Starting to stop Robotil on the current node.");
		boolean robotIsUp = false;
		String nodeIp = Base.base.getIpOfNode();
		Logging.info(nodeIp);
		String nodeOsuser = PropertyHelper.HubConfig.getPropertyValue("hub_" + hubName + "_node_osuser");
		Logging.info("nodeOsuser:" + nodeOsuser);
		String nodeOspassword = PropertyHelper.HubConfig.getPropertyValue("hub_" + hubName + "_node_ospassword");
		int count = 0;
		robotIsUp = verifyIfPrecessIsUp(nodeIp, nodeOsuser, nodeOspassword, "0.0.0.0:6666");
		while (robotIsUp && count < 3) {
			sendSSHCommandToKillProcessByPort(nodeIp, nodeOsuser, nodeOspassword, "0.0.0.0:6666", "windows");
			robotIsUp = verifyIfPrecessIsUp(nodeIp, nodeOsuser, nodeOspassword, "0.0.0.0:6666");
			count++;
		}
	}

	/**
	 * @author Fiona Zhang
	 * @Created Aug/12/2017
	 * @param requestURL
	 *            -- the http request URL
	 */
	public static String requestGetMethod(String requestURL) {
		HttpClient client = new DefaultHttpClient();
		HttpGet request = new HttpGet(requestURL);
		StringBuffer result = new StringBuffer();

		try {
			HttpResponse response = client.execute(request);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result.toString();
	}

}
