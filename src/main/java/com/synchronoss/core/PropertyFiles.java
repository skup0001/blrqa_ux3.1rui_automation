package com.synchronoss.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * Handles name-value mappings of web element locator by reading from property files.
 */
public class PropertyFiles {
    private String filename;
    public static String userInterface = "";
    public static String url="";
    
    public PropertyFiles(String file) {
        setFilename(file);
    }
    
    public String getFilename() {
        return filename;
    }
    
    public void setFilename(String filename) {
        this.filename = filename;
    }
    
    public static String getUserInterface() {
        return userInterface;
    }
    
    public static void setUserInterface(String userInterface) {
        PropertyFiles.userInterface = userInterface;
    }
    
    //SSO workaround
    public static String getURL() {
        return url;
    }
    
    //SSO workaround
    public static void setURL(String url) {
        PropertyFiles.url = url;
    }
    
    public String getPropertyValue(String key) {
    	
        Properties coreProperties = new Properties();
        Properties overridingProperties = new Properties();
        Properties merged = new Properties();
        
        String coreFileName = this.getFilename();
        String overridingFileName = "";
        
        if (userInterface.equals("sbm")) {
            overridingFileName = "SBM_OverridingUI";
        }
        /*else if (Base.browserName.equalsIgnoreCase("edge")) {
        	overridingFileName = "MicrosoftEdgeOverriding";
        } */
        else {
            overridingFileName = this.getFilename();
        }

        
        try {
            final String dir = System.getProperty("user.dir");
            
//            String resourceDir = "/resource/";
            String resourceDir = "/src/main/java/com/resource/properties/";

            File coreFile = new File(dir + resourceDir + coreFileName + ".properties");
            FileInputStream coreFileInput = new FileInputStream(coreFile);
            coreProperties.load(coreFileInput);
            coreFileInput.close();
            
            File overridingFile = new File(dir + resourceDir + overridingFileName + ".properties");
            FileInputStream overridingFileInput = new FileInputStream(overridingFile);
            overridingProperties.load(overridingFileInput);
            overridingFileInput.close();
            
            // Merge and replace values in core properties with values from 
            // overriding properties that has identical keys.
            merged.putAll(coreProperties);
            merged.putAll(overridingProperties);
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return merged.getProperty(key);
    }
    
}