package com.synchronoss.core;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.internal.Coordinates;
import org.openqa.selenium.internal.Locatable;
import org.openqa.selenium.internal.WrapsElement;

import org.openqa.selenium.JavascriptExecutor;

/**
 * A new implementation of WebElement interface to override click() method.
 * 
 * Executing the current tests on Microsoft Edge encountered an unresolved issue of 
 * the browser itself (or the MS Webdriver Server), causing clicking actions to throw 
 * an exception of "Element is Obscured". This has been tracked on Microsoft forum: 
 * https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/5238133/
 * 
 * This implementation aims to override WebElement's click() method by invoking 
 * Javascript executions from the Webdriver. However, it is only a temporary solution 
 * to the issue and may not working completely as expected.
 * 
 * @author Jerry Zhang
 */
public class WebElementForEdge implements WebElement, WrapsElement, Locatable {
	// test
	private final WebElement element;
	
	public WebElementForEdge(final WebElement webElement) {
		this.element = webElement;
	}

	@Override
	public <X> X getScreenshotAs(OutputType<X> arg0) throws WebDriverException {
		return this.element.getScreenshotAs(arg0);
	}

	@Override
	public void clear() {
		this.element.clear();
	}
	
	/*
	 * New implementation for click().
	 */
	@Override
	public void click() {
		((JavascriptExecutor)Base.base.getDriver())
				.executeScript("arguments[0].click()", this.element);
	}

	@Override
	public WebElement findElement(By arg0) {
		WebElement elem = this.element.findElement(arg0);
		
		// Should also return a WebElementForAndroid object.
		return new WebElementForEdge(elem);
	}

	@Override
	public List<WebElement> findElements(By arg0) {
		
		List<WebElement> elemsForAndroid = new ArrayList<WebElement>();
		
		// Updates all elements to be WebElementForAndroid objects.
		List<WebElement> elems = this.element.findElements(arg0);
		for (WebElement elem : elems) {
			elemsForAndroid.add(new WebElementForEdge(elem));
		}
		return elemsForAndroid;
	}

	@Override
	public String getAttribute(String arg0) {
		return this.element.getAttribute(arg0);
	}

	@Override
	public String getCssValue(String arg0) {
		return this.element.getCssValue(arg0);
	}

	@Override
	public Point getLocation() {
		return this.element.getLocation();
	}

	@Override
	public Rectangle getRect() {
		return this.element.getRect();
	}

	@Override
	public Dimension getSize() {
		return this.element.getSize();
	}

	@Override
	public String getTagName() {
		return this.element.getTagName();
	}

	@Override
	public String getText() {
		return this.element.getText();
	}

	@Override
	public boolean isDisplayed() {
		return this.element.isDisplayed();
	}

	@Override
	public boolean isEnabled() {
		return this.element.isEnabled();
	}

	@Override
	public boolean isSelected() {
		return this.element.isSelected();
	}

	@Override
	public void sendKeys(CharSequence... arg0) {
		this.element.sendKeys(arg0);
	}

	@Override
	public void submit() {
		this.element.submit();
	}

	@Override
	public WebElement getWrappedElement() {
		// NOTE: this method may not work correctly!
		return this.element;
	}

	@Override
	public Coordinates getCoordinates() {
		// NOTE: this method may not work correctly!
		return ((Locatable)this.element).getCoordinates();
	}

}
